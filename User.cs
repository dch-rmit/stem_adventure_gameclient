using System;
using System.Collection.Generic;

public class User { 

    public string name;
    public string ID;
    public Identity[] identity;

	public User(string name, string ID, Identity[] identity)
	{
        this.name = name;
        this.ID = ID;
        this.identity = identity;
	}
}

