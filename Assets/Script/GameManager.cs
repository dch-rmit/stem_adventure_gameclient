﻿using System;
using UnityEngine;

namespace Script
{
    /// <summary>
    ///  GameManager is implemented to track the current game state or the currently loaded game.
    /// </summary>
    public class GameManager
    {
        
        // The game that is open in the GamePlay window. 
        public Game CurrentGame { get; set; }

        // GameManager is a singleton. Ensure only one instance.
        private static GameManager instance;

        public static GameManager GetInstance()
        {
            return instance ?? (instance = new GameManager());
        }

        public void StartNewGame(Story story)
        {
            Debug.Log("New story name is " + story.name);
            CurrentGame = new Game(story, SceneState.getInstance());
        }

        public void SetSelectedStory(Story selectedStory)
        {
            StartNewGame(selectedStory);
        }
    }
}