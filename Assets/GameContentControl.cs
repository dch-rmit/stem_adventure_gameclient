﻿using System.IO;
using Doozy.Engine.UI;
using SourceCode.utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameContentControl : MonoBehaviour
{

	public UIView viewForthis;
    public GameObject messageBox;
    public GameObject imagePanel;
    public RectTransform selectionPanel;
    public RawImage rawImage;

    public GameObject prefabButton;

    public Animator styleAnimator;
    
    [Header("Line length")]
    [SerializeField]
    private int minSelectionTextLengthLine = 8;
    [SerializeField]
    private int maxSelectionTextLength = 44;

    [Header("Image padding on sides")]
    [SerializeField] private int imagePadding = 100;

    public void ShowView()
    {
	    viewForthis.Show();
    }
    
    public void SetSceneText(string text)
    {
            //This replacement is here because carriage returns are being interpreted as 4 tabs instead of
            // a line break. Not sure what is causing the issue but for the moment tabs will be removed to fix.
            string newString = text.Replace("\\n", "\n").Replace("\\t", "").Replace("\t", "");
            TextMeshProUGUI textMeshProUgui = messageBox.GetComponent<TextMeshProUGUI>();
            textMeshProUgui.text = newString;
    }

    public void SetupSceneImage(Stage.Image[] images)
    {
	    GameObject masterCanvas = GameObject.Find("Canvas - MasterCanvas");

        if (images.Length <= 0) {
        				HideImage ();
        				return;
        }

        imagePanel.gameObject.SetActive(true);
        RectTransform scrollViewRectTransform = GetComponent<RectTransform> ();
        var rect = scrollViewRectTransform.rect;
	    float scrollViewWidth = rect.width - 40; // minus left and right padding
	    float scrollViewHeight = rect.height;

	    //	rawImageView.gameObject.SetActive(true);
        Texture2D texture = new Texture2D(100, 100);
        string filePath = images [0].imagePath;
        byte[] bytes = File.ReadAllBytes(filePath);
        
        texture.LoadImage(bytes);
        texture.name = filePath;
	    rawImage.texture = texture;
	    rawImage.SizeToParent2(scrollViewWidth, scrollViewHeight);
	    rawImage.SetNativeSize();
	    int maxSize = (int)masterCanvas.getWidth() - imagePadding;

	    // If the image if wider than the maximum width size.
	    if (rawImage.texture.width <= maxSize) return;
	    rawImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, maxSize);
				 
	    var ratioX = (double) maxSize / rawImage.texture.width;
	    var newHeight = (int) (rawImage.texture.height * ratioX);
	    RectTransform rectTransform = rawImage.GetComponent<RectTransform>();
	    rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, newHeight);
    }

    private void HideImage()
    {
	    imagePanel.gameObject.SetActive(false);
    }

    
    public GameObject AddButton(string text, int longestLineLength)
    {
	    text = text.Trim();
	    text = UITextUtils.AdjustButtonStringForTextLength(text, minSelectionTextLengthLine, maxSelectionTextLength,
		    longestLineLength);

	    int lineCount = UITextUtils.CountLines(text);
	    GameObject button = CreateButton(text, CalculateButtonHeight(lineCount));
	    return button;
    }
    
    private float CalculateButtonHeight(int lineCount)
    {
	    return 1.0f + (lineCount >= 3 ? (lineCount - 2) * 0.5f : 0);
    }
    
    private GameObject CreateButton(string buttonText, float heightMultiply = 1.0f)
    {
	    GameObject newButton = Instantiate(prefabButton, selectionPanel);
	    newButton.GetComponentInChildren<TextMeshProUGUI>().text = buttonText;
	    newButton.transform.SetParent(selectionPanel, false);
			
	    RectTransform rectTransform = prefabButton.GetComponent<RectTransform> ();
	    float height = rectTransform.rect.height * heightMultiply;

	    Vector2 v = newButton.GetComponent<RectTransform> ().sizeDelta;
	    float width = v.x;
	    float minWidth = 100.0f;
	    if (v.x < minWidth)
	    {
		    v.x = minWidth;
	    }
	    newButton.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, height);
			
	    return newButton;
    }

    public void PlayStyle()
    {
	    styleAnimator.Play("");
    }
    
}
