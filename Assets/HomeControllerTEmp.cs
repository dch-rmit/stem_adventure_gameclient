﻿using System.Collections;
using System.Collections.Generic;
using Proyecto26;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI.Extensions;

public class HomeControllerTEmp : MonoBehaviour
{
  public GameObject bannerPrefab;
    public GameObject contentPanel;
    private List<Story> topStoriesFromStoryList;
    private List<GameObject> banners;
    private int currentTopStoryIndex = 0;
    [SerializeField] private int bannerChangeTime;
    private bool storiesLoaded;
    public GameObject fallBackBanner;
    public HorizontalScrollSnap horizontalScrollSnap;
    
    // Start is called before the first frame update
    void Start()
    {
        SetupTopStoryView();
    }
    

    public void SetupTopStoryView()
    {
        banners = new List<GameObject>();
        topStoriesFromStoryList = new List<Story>();

        LoadAllStories();
    }

    private IEnumerator StartBannerAnimation(float waitTime)
    {
        foreach (var banner in banners)
        {
            banner.SetActive(false);
        }
        banners[0].gameObject.SetActive(true);
        
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            banners[currentTopStoryIndex].SetActive(false);
            int index = GetNextStoryIndex();
            GameObject banner = banners[index];
            banner.SetActive(true);

            if (currentTopStoryIndex == (banners.Count -1))
            {
                currentTopStoryIndex = 0;
            }
            else
            {
                currentTopStoryIndex++;
            }
        }
    }

    private IEnumerator BannerSetup(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
    }


    private GameObject SetupImage(Story story)
    {
        GameObject imageView = Instantiate(bannerPrefab, contentPanel.transform);
        BannerPrefab bf = imageView.GetComponent<BannerPrefab>();
        SetUpBannerImage(bf, story);
        SetTitle(bf ,story.name);
        return imageView;
    }

    private void ResizeImage(BannerPrefab bf)
    {
        var mainTexture = bf.bannerImage.mainTexture;
        Debug.Log(mainTexture.width);
        int desiredWidth = RecalculateImageSizeBasedOnHeight(mainTexture.width, mainTexture.height,
            (int) contentPanel.getHeight());

        Debug.Log(desiredWidth);
        bf.SetWidth(desiredWidth);
    }

    private void SetTitle(BannerPrefab bf, string storyName)
    {
       bf.SetBannerTitle(storyName);
    }
     

    public int RecalculateImageSizeBasedOnHeight(int oldWidth, int oldHeight, int newHeight)
    {
        double width = oldWidth;
        double height = oldHeight;
        double aspectRatio = width / height;
        // 3. Calculate new width based on the ratio.
        int desiredWidth = (int) (newHeight * aspectRatio);
        return desiredWidth;
    }

    private void SetUpBannerImage(BannerPrefab banner, Story story)
    {
        string path = Application.persistentDataPath + "/Script/" + story.id + "/";
        Debug.Log("Background image path is: " + path);
        string jpgPath = path + "cover.jpg";
        string pngPath = path + "cover.png";
        bool jpgExsts = ImageExtensions.GetImagePath(jpgPath);
        Debug.Log("jpg exists equals " + jpgExsts + " " + jpgPath);
        bool pngExists = ImageExtensions.GetImagePath(pngPath);
        Debug.Log("png exists equals " + pngExists + " " + pngPath);

        if (jpgExsts)
        {
            banner.bannerImage.setImagePath(jpgPath);
            ResizeImage(banner);
            Debug.Log("Image loaded from " + jpgPath);
            return;
        }

        if (pngExists)
        {
            banner.bannerImage.setImagePath(pngPath);
            ResizeImage(banner);
            Debug.Log("Image loaded from " + pngPath);
            return;
        }

        StartCoroutine(
            GetTexture(ApiRoute.CoverImageUrl(story.id, StoryManager.CoverImageExtension(story)),banner)
        );
    }


    IEnumerator GetTexture(string url, BannerPrefab banner)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D tex = ((DownloadHandlerTexture) www.downloadHandler).texture;
            var sprite = Sprite.Create (tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            banner.bannerImage.sprite = sprite;
            ResizeImage(banner);
        }
    }

    private int GetNextStoryIndex()
    {
        if (currentTopStoryIndex < banners.Count - 1) {
            return currentTopStoryIndex + 1;
        }
        
        return 0;
    }
    
    private void LoadAllStories()
    {
        var requestOptions = ApiService.RequestOptions();
        RestClient.Get(requestOptions).Then(response =>
        {
            JSONNode json = JSON.Parse(response.Text);
            JSONArray storiesJson = json.AsArray;
            for (int i = 0; i < storiesJson.Count; i++)
            {
                JSONNode storyJson = storiesJson[i];
                Story story = new Story(storyJson);

                if (story.resourceInfos.Count == 0)
                {
                    continue;
                }

                topStoriesFromStoryList.Add(story);
            }

            //Set up the top stories.
            for (int i = 0; i < topStoriesFromStoryList.Count; i++)
            {
                Story story = topStoriesFromStoryList[i];
                banners.Add(SetupImage(story));
            }

            foreach (var banner in banners)
            {
               horizontalScrollSnap.AddChild(banner);

            }

            
            storiesLoaded = true;

        }).Catch(
            err => Debug.Log("A download error occurred."));
    }

    // Note: Called by UIView in scene.
    public void StartBanners()
    {
        if (storiesLoaded)
        {
            fallBackBanner.SetActive(false);
            StartCoroutine(StartBannerAnimation(bannerChangeTime));    
        }

        // If no internet fall back to default banner.
        if (banners.Count == 0)
        {
            // If stories have not been loaded set default image.
            fallBackBanner.gameObject.SetActive(true);    
        }
    }
}
