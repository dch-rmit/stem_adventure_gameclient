﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class BannerPrefab : MonoBehaviour
{
    public TextMeshProUGUI bannerTitle;
    public Image bannerImage;

    public void SetBannerTitle(string title)
    {
        bannerTitle.SetText(title);
    }

    public void SetWidth(int width)
    {
        bannerImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
    }

    public void SetHeight(int height)
    {
        bannerImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
