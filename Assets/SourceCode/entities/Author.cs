﻿using System;
using SimpleJSON;

public class Author
{
	public int id;
	public string email;
	public string name;
	public string studentId;

//	"password_digest":"$2a$10$agv1wCb/S1/PFMJww.hm/OWJOcZKbgBduT7yyFr2l6cHHM0k4nEhi",
//	"Remember_digest":null,
//	"Permission_level":"admin",
//	"created_at":"2018-06-04T05:05:14.717Z",
//	"updated_at":"2018-06-04T05:05:37.893Z",
//	"Reset_digest":null,
//	"Reset_sent_at":null

	public Author (int id, string email, string name, string studentId)
	{
		this.id = id;
		this.email = email;
		this.name = name;
		this.studentId = studentId;
	}

	public Author (JSONNode authorJson)
	{
		this.id = authorJson[ApiRoute.KEY_ID].AsInt;
		this.email = authorJson[ApiRoute.KEY_EMAIL].Value;
		this.name = authorJson[ApiRoute.KEY_NAME].Value;
		this.studentId = authorJson[ApiRoute.KEY_STUDENT_ID].Value;
	}
}

