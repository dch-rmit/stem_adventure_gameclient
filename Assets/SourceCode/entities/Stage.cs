﻿using System;
using System.Collections.Generic;

public class Stage
{
	public string id;

	public string name;

	public string description;

//	public int marks;

	public Image[] images;

	public StageSelection[] selections;

	public Stage (string id, string name, string description, Image[] images, StageSelection[] selections)
	{
		this.id = id;
		this.name = name;
		this.description = description;

		this.images = images;
		this.selections = selections;
	}


	public class Image
	{
		public string base64String = "";
		public string imagePath;
		public int x;
		public int y;

		public Image(string base64String, string imagePath, int x, int y)
		{
			this.base64String = base64String;
			this.imagePath = imagePath;
			this.x = x;
			this.y = y;
		}

	}

}

