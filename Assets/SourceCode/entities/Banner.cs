﻿using System;
using SimpleJSON;
using SourceCode.utils;

public class Banner
{
	public int id;

	public string fileName;
	public string fileType;
	public long lastUpdate;

	public string filePath;

	public Banner (JSONNode storyJson)
	{
		id = storyJson[ApiRoute.KEY_ID].AsInt;
		fileName = storyJson[ApiRoute.KEY_FN].Value;
		fileType = storyJson[ApiRoute.KEY_FT].Value;
//		this.lastUpdate = storyJson[ApiRoute.KEY_LU].Value;
		lastUpdate = Convert.ToInt64 (storyJson[ApiRoute.KEY_LU].Value);

		BannerManager bannerManager = new BannerManager();
		filePath = bannerManager.GetFilePathOfBanner (this);
	}
}

