﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game {
	
    private Story pStory;
    private Stage pCurrentStage;
    private BasicState pCurrentState;
    private bool pIsCompleted = false;

	private int marks;

    GameObject button;

    public Game(Story story, BasicState currentState)
    {
        pStory = story;
        pCurrentStage = pStory.stages[pStory.startStageId];
        pCurrentState = currentState;
    }

    public bool isValidAction()
    {
        return false;
    }

    public void selectChoice(int choice)
    {

    }

	public void updateMarks(int updatedMarkAmount)
	{
		marks += updatedMarkAmount;

		if (marks < 0) {
			marks = 0;
		} else if (marks > pStory.maxMarks) {
			marks = pStory.maxMarks;
		}
	}

	public bool isFullMarks()
	{
		return marks == pStory.maxMarks;
	}

    public bool isCompleted()
    {
        return pIsCompleted;
    }

    public void setGameCompleted()
    {
        pIsCompleted = true;
    }

    public void setCurrentStage(string stageId)
    {
        pCurrentStage = pStory.stages[stageId];
    }

    public Stage getCurrentStage()
    {
        return pCurrentStage;
    }

    public Story getStory()
    {
        return pStory;
    }

	public int getMarks()
	{
		return marks;
	}
}
