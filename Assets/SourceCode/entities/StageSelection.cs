﻿using System;

public class StageSelection
{
	public string text;
	public string nextStageId;
	public int marks;

	public StageSelection (string selectionText, int marks, string nextStageId)
	{
		this.text = selectionText;
		this.nextStageId = nextStageId;
		this.marks = marks;
	}
}

