﻿using System;
using System.Collections.Generic;

using SimpleJSON;

public class Story
{
	public int id;

	public string name;
	public string authorName;
	public string version;
	public string theme;
	public string backgroundImagePath;
	public string preface;
	public string keyLearning;

	public string lastUpdate;

	public Author author;
	public List<ResourceInfo> resourceInfos;

	public string startStageId;
	public Dictionary<string, Stage> stages;

	public int maxMarks;

	public Boolean isAds;
	
	//List of variables required by api.
	
	
	// This constructor is used in Select Stories scene
	public Story (string name, string authorName, string version, string theme, int maxMarks, string backgroundImagePath, 
		string preface, string startStageId, Dictionary<string, Stage> stages, string keyLearning)
	{
		this.name = name;
		this.authorName = authorName;
		this.version = version;
		this.theme = theme;
		this.maxMarks = maxMarks;
		this.backgroundImagePath = backgroundImagePath;
		this.preface = preface;
		this.startStageId = startStageId;
		this.stages = stages;
		this.keyLearning = keyLearning;
	}

	// This constructor is used in Download Story scene
	public Story (JSONNode storyJson)
	{
		id = storyJson[ApiRoute.KEY_ID].AsInt;
		name = storyJson[ApiRoute.KEY_N].Value;
		preface = storyJson[ApiRoute.KEY_D].Value;
		theme = storyJson[ApiRoute.KEY_T].Value;
		version = storyJson[ApiRoute.KEY_V].Value;

		lastUpdate = "" + storyJson[ApiRoute.KEY_LU].Value;

		author = new Author (storyJson[ApiRoute.KEY_A]);
		authorName = author.name;
		authorName = storyJson[ApiRoute.KEY_AUTHOR_NAME].Value;
		JSONArray resources = storyJson[ApiRoute.KEY_R].AsArray;
		resourceInfos = new List<ResourceInfo>();

		// Parse resource infos
		foreach (JSONNode resourceJson in resources) {
			ResourceInfo resInfo = new ResourceInfo (
				resourceJson[ApiRoute.KEY_FN], resourceJson[ApiRoute.KEY_FT]);

			resourceInfos.Add (resInfo);
		}
	}

	public class ResourceInfo
	{
		public string fileName;
		public string fileType;

		public ResourceInfo(string fileName, string fileType)
		{
			this.fileName = fileName;
			this.fileType = fileType;
		}
	}
}

