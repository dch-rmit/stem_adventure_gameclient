﻿using System;

public class AttemptRecord
{
	private string attemptId;
	private string storyId;
	private string userId;

	private int marks;
	private int wrongAnswerCount;

	public AttemptRecord (string attemptId, string storyId, string userId, int marks, int wrongAnswerCount)
	{
		this.attemptId = attemptId;
		this.storyId = storyId;
		this.userId = userId;
		this.marks = marks;
		this.wrongAnswerCount = wrongAnswerCount;
	}

	public string getAttemptId()
	{
		return attemptId;
	}

	public string getStoryId()
	{
		return storyId;
	}

	public string getUserId()
	{
		return userId;
	}

	public int getMarks()
	{
		return marks;
	}

	public int getWrongAnswerCount()
	{
		return wrongAnswerCount;
	}
}

