﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicState {

    static BasicState pCurrentState = null;

    protected virtual void init() {

    }

    protected virtual void deInit() {

    }

    protected virtual void show() {

    }

    protected virtual void onButtonClicked() {

    }

    public static void changeState(BasicState newState)
    {
        // Don't do anything if change state to null
        if (newState == null)
        {
            return;
        }
        if (pCurrentState != newState)
        {
            pCurrentState.deInit();
            pCurrentState = newState;
            pCurrentState.init();
        }
        pCurrentState.show();
    }

    public bool isValidAction()
    {
        return false;
    }

    // Handle android back button, usually go back to previous screen or cancel an action
    public void back()
    {

    }
}
