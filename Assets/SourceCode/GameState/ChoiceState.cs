﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceState : BasicState {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private static ChoiceState pState = null;

    public static BasicState getInstance()
    {
        if (pState == null)
        {
            pState = new ChoiceState();
        }
        return pState;
    }

    protected override void init() {
        // Init whatever here
    }

    protected override void deInit() {
        // Cleanup when no longer need
    }

    protected override void show() {
        // Implement text + graphic to be display on screen
    }

    protected override void onButtonClicked() {
        // // Implement input processing
        // if (button.name == "Choice1")
        // {
        // // Process choice here
        // }
        // else if (button.name == "Choice2")
        // {
        // // Process choice here
        // }
        // else
        // {
        // // Handle whatever invalid here
        // }
        //changeState();
    }
}
