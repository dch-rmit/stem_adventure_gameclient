﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ProgressBar;

public class ProgressDialogViewController : MonoBehaviour {

	public GameObject progressView;

	private ProgressRadialBehaviour pProgressController;
	private System.Threading.Timer pTimer;

	// Use this for initialization
	void Start () {
		progressView.transform.Find ("TextPanel").gameObject.SetActive(false);

		pProgressController = progressView.GetComponent<ProgressRadialBehaviour> ();
//		pProgressController.OnCompleteMethods = new ProgressBar.Utils.OnCompleteEvent() {
//			pProgressController.reset();
//		};

		startProgress ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void startProgress()
	{
		var startTimeSpan = TimeSpan.Zero;
		var periodTimeSpan = TimeSpan.FromSeconds(0.025);

		pTimer = new System.Threading.Timer((e) =>
			{
				if (pProgressController.isDone) {
//					pProgressController.Value = 0;
//					pProgressController.TransitoryValue = 0;
					pProgressController.reset();
					return;
				}

				pProgressController.IncrementValue(1);

			}, null, startTimeSpan, periodTimeSpan);
	}

	public void dismiss()
	{
		if (pTimer != null) {
			pTimer.Dispose ();
		}
	}
}
