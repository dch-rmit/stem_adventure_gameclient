﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PhoneTemplateViewController : MonoBehaviour {

	public Button menuButton;

	public GameObject sideMenuPanel;

	public Button infoButton;
	
	public Canvas Canvas;

	public GameObject infoPanel;
	
	// Use this for initialization
	void Start () {
		sideMenuPanel.SetActive (false);
        
		menuButton.onClick.AddListener(() => {
			sideMenuPanel.SetActive (true);
		});
		
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
