﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneViewController : MonoBehaviour {

	// Use this for initialization
	void Start () {

		// Remove sample story if exists
		StoryManager storyManager = new StoryManager();
		string destFolder = storyManager.getRootStoryFolderPath () + "/sample";
		if (Directory.Exists(destFolder)) {
			Directory.Delete (destFolder, true);
		}

        //Prepare the story folder

        string destPlistPath = destFolder + "/sample.plist";

        // Create the sample folder if not existed
        if (!Directory.Exists(destFolder))
        {
            Directory.CreateDirectory(destFolder);
        }

        // If the sample story has not been copied to app folder yet
        if (!File.Exists(destPlistPath))
        {
            // Copy sample stories into application folder first
            Object[] files = Resources.LoadAll("sample_stories");

            foreach (Object o in files)
            {
                Debug.Log("Resources file: " + o.name);

                if (o is TextAsset)
                {
                    TextAsset ta = (TextAsset)o;
                    File.WriteAllBytes(destPlistPath, ta.bytes);
                }
                else if (o is Texture2D)
                {
                    Texture2D texture = (Texture2D)o;
                    File.WriteAllBytes(destFolder + "/" + texture.name + ".png", texture.EncodeToPNG());
                }

            }
        }

        Router.toHome ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
