﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using SimpleJSON;

public class DownloadStoryViewController : MonoBehaviour {

	public GameObject chooseThemePanel;
	public Button scienceButton;
	public Button technologyButton;
	public Button engineeringButton;
	public Button mathematicsButton;

	public GameObject chooseStoryPanel;
	public Text selectedThemeTitle;
	public ScrollRect scrollView;
	public GameObject tempStorySelectionItem;

	private List<GameObject> storyItemViews;

	private Dictionary<string, string> themeMap;
	private Dictionary<string, List<Story>> allStories;
	private List<Story> currentStoryList;

	private string selectedTheme;

	// Use this for initialization
	void Start () {

		storyItemViews = new List<GameObject> ();
		allStories = new Dictionary<string, List<Story>> ();
//		stories = new List<Story> ();

		allStories.Add (StoryManager.ThemeScience, new List<Story> ());
		allStories.Add (StoryManager.ThemeTechnology, new List<Story> ());
		allStories.Add (StoryManager.ThemeEngineering, new List<Story> ());
		allStories.Add (StoryManager.ThemeMath, new List<Story> ());

//		currentStoryList = new List<Story> ();


		selectedThemeTitle.text = "Choose a theme that you would like to download";
		chooseStoryPanel.SetActive (false);

		scienceButton.onClick.AddListener(() => {
			toChooseStoryTheme(StoryManager.ThemeScience);
		});

		technologyButton.onClick.AddListener(() => {
			toChooseStoryTheme(StoryManager.ThemeTechnology);
		});

		engineeringButton.onClick.AddListener(() => {
			toChooseStoryTheme(StoryManager.ThemeEngineering);
		});

		mathematicsButton.onClick.AddListener(() => {
			toChooseStoryTheme(StoryManager.ThemeMath);
		});


		setupThemeMap ();
		loadAllStories ();
//		updateStoryListView();


//		#if UNITY_ANDROID || UNITY_IOS
//		#else
//		PCTemplateViewController templateVC = chooseThemePanel.transform.root.Find("PCTemplatePanel")
//			.GetComponentInChildren<PCTemplateViewController>();
//		templateVC.setButtonHighlighted(templateVC.downloadButton);
//		#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// TODO: this should be update later
	private void setupThemeMap()
	{
		StoryManager storyManager = new StoryManager ();
		themeMap = storyManager.CreateThemeMap ();
	}

	private void toChooseStoryTheme(string selectedTheme)
	{
		if (allStories.ContainsKey(selectedTheme)) {
			List<Story> tempList = allStories [selectedTheme];

			if (tempList.Count == 0) {
				string message = "There is no story in " + selectedTheme 
					+ " Theme at the moment, but you can submit your own story!";

				MessageDialogViewController.show (message);
				return;
			}
		}

		if (this.selectedTheme != selectedTheme) {
			this.selectedTheme = selectedTheme;
			selectedThemeTitle.text = selectedTheme;

			currentStoryList = allStories [selectedTheme];
			updateStoryListView ();
		}

		chooseThemePanel.SetActive (false);
		chooseStoryPanel.SetActive (true);
	}

	private void loadAllStories()
	{
//		StoryManager storyManager = new StoryManager ();
//		stories = storyManager.getAllStories();
//
//		int i = 1;
//		foreach (Story story in stories) {
//			story.id = i;
//			i += 1;
//		}


		ApiService service = new ApiService ();
		StartCoroutine (service.DoGetMethod(ApiRoute.ALL_STORIES_URL,
			(w) => { // On success
				string jsonString = w.downloadHandler.text;
				JSONNode json = JSON.Parse(jsonString);
				JSONArray storiesJson = json[ApiRoute.KEY_STORIES].AsArray;

				for (int i = 0; i < storiesJson.Count; i++)
				{
					JSONNode storyJson = storiesJson[i];
					Story story = new Story(storyJson);

					Debug.Log ("ID: " + story.id + " - Version: " + story.version + " - Last Update: " + story.lastUpdate);

					if (story.resourceInfos.Count == 0) {
						Debug.Log("Story has no resource");
						continue; 
					}

					List<Story> storyList;

					if (themeMap.ContainsKey(story.theme)) {
						storyList = allStories[themeMap[story.theme]];
					} else {
						storyList = allStories[StoryManager.ThemeScience];
					}

					storyList.Add(story);
				}

//				updateStoryListView();
			},
			(w) => { // On error
				
			}));
	}

	private void updateStoryListView()
	{
		for(int i = 0; i < storyItemViews.Count; i++)
		{
			GameObject item = storyItemViews [i];

			item.transform.SetParent (null);
			GameObject.Destroy(item);
		}

		foreach (Story story in currentStoryList) {
			setupStoryItemView (story);
		}

		tempStorySelectionItem.SetActive (false);
	}

	private void setupStoryItemView(Story story)
	{
//		Debug.Log ("****** Story name: " + story.name);

		GameObject storyItemView = Instantiate(tempStorySelectionItem, 
			tempStorySelectionItem.transform.position, tempStorySelectionItem.transform.rotation);
		
		Button buttonComponent = storyItemView.GetComponent<Button> ();
		buttonComponent.enabled = false;

		string title = story.name;
		string authorName = story.authorName;
		string theme = story.theme;
		string version = story.version;
//			int achievedScore = 0;
//			int maxScore = story.maxMarks;

		string description = "By " + authorName + "\nVersion " + version;

		string coverImageExt = "png";

		// Find the extension of the cover image
		foreach (Story.ResourceInfo res in story.resourceInfos) {
			if (res.fileName == "cover") {
				coverImageExt = res.fileType;
				break;
			}
		}

		string coverImageUrl = ApiRoute.DOWNLOAD_STORY_URL + "?id=" + story.id + "&name=cover&type=" + coverImageExt;

//			string score = "Score: " + achievedScore + "/" + maxScore;

		storyItemView.transform.Find("StoryTitle").GetComponent<Text>().text = title;
		storyItemView.transform.Find("StoryDescription").GetComponent<Text>().text = description;
//		storyItemView.transform.Find("StoryTheme").GetComponent<Text>().text = theme;
//			storyItemView.transform.Find("StoryScore").GetComponent<Text>().text = score;
//			storyItemView.transform.Find("Badge").;

//			storyItemView.transform.Find ("StoryScore").gameObject.SetActive (false);


//		storyItemView.transform.Find("StoryImage").GetComponent<Image>().text = description;

		Image image = storyItemView.transform.Find("StoryImage").GetComponent<Image> ();
		image.preserveAspect = true;

		StartCoroutine(
			image.setImageURL(coverImageUrl)
		);

		storyItemView.transform.SetParent(scrollView.content, false);

		// Setup the download & remove button

		Button removeButton = storyItemView.transform.Find("RemoveButton").GetComponent<Button>();
		Button downloadButton = storyItemView.transform.Find("DownloadButton").GetComponent<Button>();
		Text downloadButtonText = downloadButton.transform.Find("Text").GetComponent<Text>();

		string lastUpdateTime = PrefUtils.loadDownloadedStoryLastUpdateTime (story.id);

		Debug.Log ("Story ID: " + story.id);
		Debug.Log ("Loaded lastUpdateTime: " + lastUpdateTime);
		Debug.Log ("Last Update: " + story.lastUpdate);

		removeButton.gameObject.SetActive (false);

		// 1527793830 - 1527793830
		if (lastUpdateTime.Length > 0) { // This story was already downloaded

			if (lastUpdateTime == story.lastUpdate) { // The current version is up-to-date
				downloadButtonText.text = "Downloaded";
				downloadButton.enabled = false;
				buttonComponent.enabled = true;
			} else { // New version available
				downloadButtonText.text = "Update";
			}

			removeButton.gameObject.SetActive (true);

		} else { // This story is not downloaded yet
			downloadButtonText.text = "Download";
		}

		// Set listener
		downloadButton.onClick.AddListener(() => {
//			Debug.Log("****** Button Clicked ****** Story Name: " + story.name);

			downloadButtonText.text = "Downloading..";
			downloadButton.enabled = false;

			ApiService service = new ApiService();

			service.DownloadStory(this, story,
				(w) => { // On success
					downloadButtonText.text = "Downloaded";
					buttonComponent.enabled = true;
					removeButton.gameObject.SetActive (true);
				},
				(w) => { // On error
					downloadButtonText.text = "Download";
					downloadButton.enabled = true;
				});
		});

		removeButton.onClick.AddListener (() => {

			MessageDialogViewController.show(
				"Do you want to remove this story?", true,
				() => {
					StoryManager sm = new StoryManager();
					sm.removeStory(story.id);

					buttonComponent.enabled = false;
					removeButton.gameObject.SetActive (false);

					downloadButtonText.text = "Download";
					downloadButton.enabled = true;

					MessageDialogViewController.dismiss();
				});
		});


		buttonComponent.onClick.AddListener (() => {
			Story selectedStory = new StoryManager().getStoryById(story.id);
			Router.toGameplay(selectedStory);
		});

		storyItemViews.Add (storyItemView);
	}
}
