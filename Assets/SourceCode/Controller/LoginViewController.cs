﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Networking;

using SimpleJSON;

public class LoginViewController : MonoBehaviour {

	public InputField emailField;
	public InputField passwordField;
	public Button okButton;
	public Button signUpButton;

	// Use this for initialization
	void Start () {
//		PlayerPrefs.DeleteAll ();

		okButton.onClick.AddListener(() => {
			okBtnClicked();
		});
			
		signUpButton.onClick.AddListener(() => {
			Router.toSignUp();
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void okBtnClicked()
	{
		string email = emailField.text;
		string password = passwordField.text;

		Debug.Log ("Login - Ok clicked - " + email + " - " + password);

		string alertMessage = "";

		if (email == "") {
			alertMessage += "Email cannot be empty\n";
		}
		if (password == "") {
			alertMessage += "Password cannot be empty\n";
		}

		// Input is invalid. Display alert and return.
		if (alertMessage.Length > 0) {
			MessageDialogViewController.show (alertMessage);
			return;
		}


		okButton.enabled = false;
//		ProgressUtils.showProgressDialog ();

		ApiService apiService = new ApiService ();

		StartCoroutine(apiService.HandleLogin (email, password,
			(w) => { // Success listener
				string jsonString = w.downloadHandler.text;
				JSONNode json = JSON.Parse(jsonString);
				int userId = json [ApiRoute.KEY_ID].AsInt;
				string session = json [ApiRoute.KEY_SESSION].Value;

				PrefUtils.saveUserId (userId);
				PrefUtils.saveSession (session);
				PrefUtils.save();

				Router.toHome();
			},
			(w) => { // Error listener
				Debug.Log("W: " + w.downloadHandler.text);
				okButton.enabled = true;

				string jsonString = w.downloadHandler.text;

				if (jsonString != "")
				{
					JSONNode json = JSON.Parse(jsonString);
					string message = json [ApiRoute.KEY_ERRORS].Value;

					MessageDialogViewController.show (message);
				}
			}
		));


		// The following codes are for testing purpose
//		bool valid = true;
//
//		if (valid) {
//			Router.toSelectStories();
//		} else {
//
//		}
	}
}
