﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PhoneStoriesViewController : MonoBehaviour {

	public static string selectedTheme = "";

	public Canvas canvas;
	public Text topTitle;
	public ScrollRect scrollView;
	public GameObject tempStorySelectionItem;

	private Dictionary<string, string> themeMap;
	private List<Story> stories;

	// Use this for initialization
	void Start () {
		StoryManager storyManager = new StoryManager ();
		themeMap = storyManager.CreateThemeMap ();

		topTitle.text = selectedTheme;

		LoadStory ();
		UpdateStoryListView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void LoadStory()
	{
		StoryManager storyManager = new StoryManager ();

		Debug.Log ("START scanning story folder and parsing story files");
		Debug.Log ("Application.dataPath - " + Application.dataPath);
		Debug.Log ("Application.persistentDataPath - " + Application.persistentDataPath);

        
        List<Story> allStories = storyManager.GetAllStories();
        stories = new List<Story>();

        foreach (Story s in allStories)
        {
            string realTheme = StoryManager.ThemeScience;

            if (themeMap.ContainsKey(s.theme))
            {
                realTheme = themeMap[s.theme];
            }

            if (realTheme == selectedTheme)
            {
                stories.Add(s);
            }
        }

        stories = storyManager.getStoriesByTheme (selectedTheme);

		Debug.Log ("DONE scanning and parsing");
		Debug.Log ("Number of stories: " + stories.Count); 
	}
     
	private void UpdateStoryListView()
	{
		foreach (Story story in stories) {
//			Debug.Log ("****** Story name: " + story.name);
//			Debug.Log ("****** Number of stages: " + story.stages.Count);

			string imagePath = story.backgroundImagePath;
			int achievedScore = 0;
			int maxScore = story.maxMarks;

            //Set up the storyItemView with the tempStorySelectionItem as a placeholder.
			GameObject storyItemView = Instantiate(tempStorySelectionItem, 
				tempStorySelectionItem.transform.position, tempStorySelectionItem.transform.rotation);

			Image imageView = storyItemView.transform.Find ("StoryImage").GetComponent<Image> ();

			// TODO: Retrieve achievedScore for this story here

			string score = "Score: " + achievedScore + "/" + maxScore;

			storyItemView.transform.Find("StoryTitle").GetComponent<Text>().text = story.name;
			storyItemView.transform.Find("StoryDescription").GetComponent<Text>().text = story.preface;
			storyItemView.transform.Find("StoryTheme").GetComponent<Text>().text = story.theme;
//			storyItemView.transform.Find("StoryScore").GetComponent<Text>().text = score;

			int userId = PrefUtils.loadUserId ();
			int marks = PrefUtils.loadFullMarkStoryAttempt (story.id, userId);
			bool badgeShown = marks > 0;
			storyItemView.transform.Find("Badge").gameObject.SetActive(badgeShown);

			// If the cover image path of the story is available, set the image to the image view
			if (imagePath != null && imagePath != "") {
				imageView.setImagePath (imagePath);
			} else { // Otherwise, set default image according to the story's theme
				StoryManager sm =  new StoryManager();
				imageView.setImagePath(sm.getPathToThemeImage(story.theme));
			}

			imageView.preserveAspect = true;

			storyItemView.transform.Find ("StoryScore").gameObject.SetActive (false);

			storyItemView.transform.SetParent(scrollView.content, false);

			storyItemView.transform.GetComponent<Button>().onClick.AddListener(() => {
				Debug.Log("****** Button Clicked ****** Story Name: " + story.name);

//				GameplayViewController.setSelectedStory(story);
				Router.toGameplay(story);
			});
		}

		tempStorySelectionItem.SetActive (false);

		Debug.Log ("List View - Items Count: " + scrollView.content.childCount);
	}
}
