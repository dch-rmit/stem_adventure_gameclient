﻿using SourceCode.utils;
using UnityEngine;
using UnityEngine.UI;

namespace SourceCode.Controller
{
    public class DownloadStoryItemViewController : MonoBehaviour
    {
        public Button RemoveButton { get; private set; }
        public Button DownloadButton { get; set; }
        public Text DownloadButtonText { get; set; }

        public Button ButtonComponent { get; set; }
        public Image Image { get; set; }

        public void Start()
        {
            // Get all the different components that form the StoryItemView
            RemoveButton = transform.Find("RemoveButton").GetComponent<Button>();
            DownloadButton = transform.Find("DownloadButton").GetComponent<Button>();
            DownloadButtonText = DownloadButton.transform.Find("Text").GetComponent<Text>();
            ButtonComponent = GetComponent<Button>();
            Image = transform.Find("StoryImage").GetComponent<Image>();
        }

        // Initialize the Name and authorship of the story.
        private void SetStoryItemAttributeViews(Story story)
        {
            transform.Find("StoryTitle").GetComponent<Text>().text = story.name;
            transform.Find("StoryDescription").GetComponent<Text>().text =
                "By " + story.authorName + "\nVersion " + story.version;
        }

        public void PreserveImageAspect(bool b)
        {
            Image.preserveAspect = b;
        }

        public void SetStoryAvailable()
        {
            DownloadButtonText.text = Constants.DownloadButtonSetUpdate;
        }

        public void SetStory(Story story)
        {
            SetUrlForImageComponent(story);
            SetStoryItemAttributeViews(story);
            // Button starts disabled.
            ButtonComponent.enabled = false;
            // Remove button starts disabled until can confirm story downloaded.
            RemoveButton.gameObject.SetActive(false);

            // Set aspect for item image.
            PreserveImageAspect(true);
        }

        private void SetUrlForImageComponent(Story story)
        {
            StartCoroutine(
                Image.setImageURL(ApiRoute.CoverImageUrl(story.id, StoryManager.CoverImageExtension(story)))
            );
        }
    }
}