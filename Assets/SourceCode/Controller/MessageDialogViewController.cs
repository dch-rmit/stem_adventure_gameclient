﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MessageDialogViewController : MonoBehaviour {

	public static OnOk onOk;
	public static string message;
	public static bool isConfirmDialog;

	public Text messageView;
	public Button okButton;
	public Button cancleButton;

	// Use this for initialization
	void Start () {
		messageView.text = message;
		cancleButton.gameObject.SetActive (isConfirmDialog);

		okButton.onClick.AddListener(() => {
			if (onOk != null) {
				onOk();
			}

			onOk = null;

			if (!isConfirmDialog) {
				dismiss();
			}
		});

		cancleButton.onClick.AddListener(() => {
			dismiss();
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void dismiss()
	{
		SceneManager.UnloadSceneAsync("MessageDialog");
	}

	public static void show(string message, bool isConfirmDialog = false, OnOk onOk = null) {
		MessageDialogViewController.message = message;
		MessageDialogViewController.isConfirmDialog = isConfirmDialog;
		MessageDialogViewController.onOk = onOk;
		SceneManager.LoadScene ("MessageDialog", LoadSceneMode.Additive);
	}

	public delegate void OnOk();
}
