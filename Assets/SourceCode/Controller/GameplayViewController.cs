﻿using System;
using Script;
using UnityEngine;

namespace SourceCode.Controller
{
	public class GameplayViewController : MonoBehaviour {

		public static Game PCurrentGame { get; private set; }

		private GameplayUIUpdater updater;
		private string pCurrentStageName = "";

		
		// Use this for initialization
		void Start ()
		{
			PCurrentGame = GameManager.GetInstance().CurrentGame;
			//Set screen orientation to auto. Can got either portrait or landscape.
			Screen.orientation = ScreenOrientation.AutoRotation;
		
			if (PCurrentGame == null || PCurrentGame.getStory () == null) {
				throw new Exception("Story should not be null");
			}

		}
		
	}
}
