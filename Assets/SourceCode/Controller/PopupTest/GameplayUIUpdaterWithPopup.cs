﻿using System.Collections.Generic;
using System.IO;
using Doozy.Engine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

namespace SourceCode.Controller.PopupTest
{
	// ReSharper disable once InconsistentNaming
	public class GameplayUIUpdaterWithPopup : MonoBehaviour
	{ 
		public ScrollRect scrollView;
		public Text title;
		public TextMeshProUGUI messageBox;
		public RawImage rawImageView;
		public RectTransform imagePanel;
		public Button options;

		public GameObject sampleButtonPrefab;
		public void Start()
		{
			options.onClick.AddListener(ShowPopup);
		}

		// Use this for initialization
		public void UpdateTitle(string text)
		{
			title.text = text;
			title.enabled = !string.IsNullOrEmpty(text);
		}

		// Updates the main Text box.
		public void UpdateMessageBox(string text)
		{
			
			//This replacement is here because carriage returns are being interpreted as 4 tabs instead of
			// a line break. Not sure what is causing the issue but for the moment tabs will be removed to fix.
			string newString = text.Replace("\\n", "\n").Replace("\\t", "").Replace("\t", "");
			
			messageBox.text = newString;
		}

		public void SetupImages(Stage.Image[] images)
		{
			Debug.Log(images.Length + " in setup images");
			
			if (images.Length <= 0) {
				HideImage ();
				return;
			}

			imagePanel.gameObject.SetActive(true);
			RectTransform scrollViewRectTransform = scrollView.GetComponent<RectTransform> ();
			var rect = scrollViewRectTransform.rect;
			float scrollViewWidth = rect.width - 40; // minus left and right padding
			float scrollViewHeight = rect.height;

			Texture2D texture = new Texture2D(100, 100);
			string filePath = images [0].imagePath;
			byte[] bytes = File.ReadAllBytes(filePath);

			texture.LoadImage(bytes);
			texture.name = filePath;
			float imageWidth = rawImageView.texture.width;
			float imageheight = rawImageView.texture.height;
			Debug.Log("image with is: " + imageWidth);
			Debug.Log("image height is: "  + imageheight);

			//Maximum image height.
			const float maxImageHeight = 1000.0f;
			const float maxImageWidth = 600.0f;
			if (scrollViewHeight > maxImageHeight)
			{
				float percentageheightofmaxsize = maxImageHeight / scrollViewHeight;
				scrollViewHeight = maxImageHeight;
				scrollViewWidth *= percentageheightofmaxsize;
			} 
		
			rawImageView.texture = texture;
			rawImageView.SizeToParent2 (scrollViewWidth, scrollViewHeight);
		
			var imageTransform = rawImageView.GetComponent<RectTransform>();
			imagePanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageTransform.rect.height);
			
			//rawImageView.GetComponent<RectTransform>().sizeDelta = new Vector2(600, 400);
		}

		private void HideImage()
		{
			imagePanel.gameObject.SetActive (false);
//			rawImageView.gameObject.SetActive(false);
		}
		
		// Start is called before the first frame update
		//TODO Separate out smaller methods from this. Too long method.
		private void ShowPopup()
		{
			//get a clone of the UIPopup, with the given PopupName, from the UIPopup Database 
			UIPopup popup = UIPopup.GetPopup("OptionChooser");
			List<UIButton> buttons = new List<UIButton>();
			//make sure that a popup clone was actually created
			if (popup == null)
				return;

			Game currentGame = GameplayViewControllerPopup.PCurrentGame;
			Stage stage = currentGame.getCurrentStage();
			StageSelection[] selections = stage.selections;
			foreach (var selection in selections)
			{
				string nextStageId = selection.nextStageId;
				var newButton = AddNewButton(popup, selection.text.Trim());
				
				buttons.Add(newButton.GetComponent<UIButton>());
				// Set listener
				if (string.IsNullOrEmpty(nextStageId) || nextStageId == "-1") {
					newButton.GetComponent<Button>().onClick.AddListener(() => {
						StoryEndViewController.currentGame = currentGame;
					
						SceneManager.LoadScene("StoryEnd", LoadSceneMode.Additive);
						currentGame.setGameCompleted();
						
						//Hide the popup after resetting view.
						popup.Hide();
					});
				} else
				{
					var selection1 = selection;
					newButton.GetComponent<Button>().onClick.AddListener(() => {
						currentGame.updateMarks(selection1.marks);
						currentGame.setCurrentStage(nextStageId);
						
						//Stage debug details
						Debug.Log("-----------------------------");
						Debug.Log("Stage debug details to follow.");
						Debug.Log(stage.images.Length);
						if (stage.images.Length > 0)
						{
							Debug.Log(stage.images[0].imagePath);
							
						}
						Debug.Log(stage.description);
						Debug.Log(stage.id);
						Debug.Log(stage.name);
						Debug.Log("-----------------------------");

						UpdateTitle("Step: " + currentGame.getCurrentStage().id);
						UpdateMessageBox(currentGame.getCurrentStage().description);
						SetupImages (currentGame.getCurrentStage ().images);

						//Hide the popup after resetting view.
						popup.Hide();
					});
				}
			}
			
			popup.Show(); //show the popup
		}

		private GameObject AddNewButton(UIPopup popup, string buttonText)
     		    {
	                PopupScript popupScript = popup.gameObject.GetComponent<PopupScript>();
	                UIButton button = sampleButtonPrefab.GetComponent<UIButton>();
					button.SetLabelText(buttonText);
					GameObject sampleB = Instantiate(sampleButtonPrefab, popupScript.contentPanel.transform, false);
					return sampleB;
					
                }
	}

	
}
 