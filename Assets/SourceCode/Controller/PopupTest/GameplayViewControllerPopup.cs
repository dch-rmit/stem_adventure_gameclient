﻿using Doozy.Engine.Nody;
using UnityEngine;

namespace SourceCode.Controller.PopupTest
{
	public class GameplayViewControllerPopup : MonoBehaviour {

//	private static bool pTesting = true;

		private static Game s_pCurrentGame;

		private GameplayUIUpdaterWithPopup updater;
		private string pCurrentStageName = "";

		public GraphController graphController;

		// Use this for initialization
		void Start ()
		{

			//Set screen orientation to auto. Can got either portrait or landscape.
			Screen.orientation = ScreenOrientation.AutoRotation;
		
			if (s_pCurrentGame == null || s_pCurrentGame.getStory () == null) {
				graphController.GoToNodeByName("AllStories");
			}

			updater = gameObject.GetComponent<GameplayUIUpdaterWithPopup>();
			UpdateTitle();

		
			if (s_pCurrentGame == null)
			{
				Debug.Log ("Current game is NULL");
				return;
			}


			if (s_pCurrentGame.isCompleted()) // If current game is completed
			{

			} 
			else
			{
				Stage stage = s_pCurrentGame.getCurrentStage();

				if (pCurrentStageName == stage.name) return;
				pCurrentStageName = stage.name;

				UpdateText ();
				Debug.Log(stage.images.Length);
				Debug.Log(stage.description);
				updater.SetupImages (stage.images);
			}

		}

		public static void SetSelectedStory(Story story)
		{
			s_pCurrentGame = new Game(story, SceneState.getInstance());
		}

		public static Game PCurrentGame
		{
			get => s_pCurrentGame;
		}

		private void UpdateText()
		{
			updater.UpdateMessageBox(s_pCurrentGame.getCurrentStage().description);
		}

		private void UpdateTitle()
		{
			updater.UpdateTitle("Step: " + s_pCurrentGame.getCurrentStage().id);
		}
	}
}
