﻿using System.Collections.Generic;
using Doozy.Engine.UI;
using Script;
using SourceCode.utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SourceCode.Controller
{
	public class GameplayUIUpdater : MonoBehaviour
	{

		public ScrollRect scrollView;
		public TextMeshProUGUI title;
		public RectTransform viewport;
		public GameObject contentPrefab;

		private List<GameObject> contentPanelList = new List<GameObject>();
		
		private void UpdateTitle(string text)
		{
			title.text = text;
			title.enabled = !string.IsNullOrEmpty(text);
		}

		private void  SetupSelectionButtons2(StageSelection[] selections, GameContentControl newPanel)
		{
			
			// Find longest text
			int longestLineLength = UITextUtils.FindLongestStringinSelectionArray(selections);
			
			foreach (var selection in selections)
			{
				string nextStageId = selection.nextStageId;

				var newButton = newPanel.AddButton(selection.text, longestLineLength);

				// Set listener
				if (string.IsNullOrEmpty(nextStageId) || nextStageId == "-1") {
					EndGameListener(newButton);
				} else
				{
					newButton.GetComponent<Button>().onClick.AddListener(() =>
					{
						Game currentGame = GameManager.GetInstance().CurrentGame;
						currentGame.setCurrentStage(nextStageId);
						SetUpNewContentPanel(currentGame);

						//Remove the old panel.
						DestroyOldPanel();

					});
				}
			}
		}

		private GameObject currentPanel;
		private GameObject nextPanel;
		private GameObject oldPanel;

		/// <summary>
		/// Set up the UI for a new game.
		/// </summary>
		/// <param name="game"></param>
		public void SetUpNewGameUI(Game game)
		{
			string nextStageID = game.getStory().startStageId;
			//Set current stage to the first stage.
			game.setCurrentStage(nextStageID);
			foreach (var panel in contentPanelList)
			{
				Destroy(panel);
			}

			
			SetUpNewContentPanel(game);
		}

		//This is to replace SetupScene.

		private void SetUpNewContentPanel(Game currentGame)
		{
			// If not the first panel set the nextPanel variable to be the current one.
			if (nextPanel !=null)
			{
				oldPanel = nextPanel;
			}

			nextPanel = Instantiate(contentPrefab, viewport);
			GameContentControl gameContentControl = nextPanel.GetComponent<GameContentControl>();
			nextPanel.transform.SetParent(viewport);
			Stage stage = currentGame.getCurrentStage();
			UpdateTitle(currentGame.getStory().name);
			gameContentControl.SetSceneText(currentGame.getCurrentStage().description);
			gameContentControl.SetupSceneImage(stage.images);
			SetupSelectionButtons2(stage.selections, gameContentControl);
			scrollView.content = nextPanel.GetComponent<RectTransform>();
			
			//Fade effects
			gameContentControl.viewForthis.Hide();
			gameContentControl.viewForthis.Show();
			
			contentPanelList.Add(nextPanel);
		}

		private void EndGameListener(GameObject button)
		{
			Game currentGame = GameManager.GetInstance().CurrentGame;
			button.GetComponent<Button>().onClick.AddListener(() => {
				StoryEndViewController.currentGame = currentGame;
				UIPopup storyEndPopup = UIPopup.GetPopup("StoryEnd");
				storyEndPopup.Show();
				currentGame.setGameCompleted();
			});
		}

		public void ScrollToTop()
		{
			scrollView.normalizedPosition = new Vector2(0, 1);
		}

		public void ScrollToBottom()
		{
			scrollView.normalizedPosition = new Vector2(0, 0);
		}

		private void DestroyOldPanel()
		{
			if (oldPanel!=null)
			{
				Destroy(oldPanel);
			}
		}

		private void Update()
		{
			if (scrollView.verticalScrollbar.isActiveAndEnabled && moreShown == false)
			{
				scrollObject.alpha = 1;
				moreShown = true;
			}

			 
			if (scrollView.verticalNormalizedPosition < 0.04f)
			{
				scrollObject.alpha = 0;
				moreShown = false;
			}
		}

		private bool moreShown;

		public CanvasGroup scrollObject;
	}
}
