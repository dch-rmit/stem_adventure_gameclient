﻿using System.Collections;
using System.Collections.Generic;
using SourceCode.utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class StoryEndViewController : MonoBehaviour {

	public static Game currentGame = null;

	public Text messageView;
	public RawImage badge;
	public Button okButton;
	public Button submitButton;
	

	// Use this for initialization
	void Start () {
		if (badge != null) badge.gameObject.SetActive (true);
		submitButton.gameObject.SetActive (false);
		
		okButton.setText("Back to Home Screen");
		okButton.onClick.AddListener(() => {
			Router.toHome ();
		});
	}

	private void SubmitButtonClicked()
	{
		int storyId = currentGame.getStory ().id;
		int userId = PrefUtils.loadUserId ();
		int marks = currentGame.getMarks ();
		int wac = 0;

		ApiService service = new ApiService ();

		StartCoroutine (service.SubmitAttempRecord(storyId, userId, marks, wac,
			(w) => { // Success

			},
			(w) => { // Error

			}
		));
		
		Router.toHome ();
	}
}
