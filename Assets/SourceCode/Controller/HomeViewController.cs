﻿using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using SourceCode.utils;
using UnityEngine;
using UnityEngine.UI;

namespace SourceCode.Controller
{
	public class HomeViewController : MonoBehaviour {

		private const int SCROLL_INTERVAL = 5;
		private const float SCROLL_ANIMATION_DURATION = 0.5f;

		public RectTransform bannerImagePanel;
		public RectTransform storyNamePanel;
		public Text storyNameLabel;
		public Image sampleTopStoryImage;
		
//	public Text topStoryDescriptionView;

		private List<RectTransform> dots;
		private List<Image> topStoryImages;
		private int currentTopStoryIndex;
		private int nextTopStoryIndex;
		private bool autoScrollPaused;
		private bool animationRunning;

		private List<Story> topStories;
		private List<Banner> banners;

		public Text topTitleView;

		// Use this for initialization
		void Start () {
			banners = new List<Banner> ();

//		topStoryDescriptionView.text = 
//			"The reader assumes the role of a character and by making STEM choices, " +
//			"determines the main character's actions and the plot's outcome.";
		
			LoadTopStories ();
			SetupTopStoryView ();
			LoadAds ();

			UpdateRelatingTextViews ();

			if (topTitleView != null)
			{
				topTitleView.gameObject.SetActive (false);
			}
		
			InvokeRepeating ("ScrollToNextImage", SCROLL_INTERVAL, SCROLL_INTERVAL);
		}
		
		private void LoadTopStories() 
		{
			StoryManager storyManager = new StoryManager ();
			topStories = storyManager.GetTopStoriesFromStoryList ();
		}

		private void LoadAds()
		{
			ApiService service = new ApiService ();

			// Get JSON of list of ads
			StartCoroutine (service.DoGetMethod(ApiRoute.ALL_ADS_URL,
				(w) => { // On success
					string jsonString = w.downloadHandler.text;
					JSONNode json = JSON.Parse(jsonString);
					JSONArray bannersJson = json[ApiRoute.KEY_BANNERS].AsArray;
					long latestBannerUpdate = PrefUtils.loadBannerLastUpdateTimeAsLong();
					long newLastUpdate = 0;

					Debug.Log("Saved Last Update: " + latestBannerUpdate);

					// Parse JSON array
					for (int i = 0; i < bannersJson.Count; i++)
					{
						JSONNode bannerJson = bannersJson[i];
						Banner banner = new Banner(bannerJson);

						Debug.Log ("ID: " + banner.id + " - FN: " + banner.fileName + " - Last Update: " + banner.lastUpdate);

						// Check for new last update time
						if (latestBannerUpdate < banner.lastUpdate) {
							newLastUpdate = banner.lastUpdate;
						}

						banners.Add(banner);

						// Prepare corresponding image view and dot
						Image imageView = SetupImageView();

						imageView.gameObject.SetActive(false);

						topStoryImages.Add(imageView);
						
					}

					Debug.Log("Picked Last Update: " + newLastUpdate);

					// Need update, reload the whole ads feature
					if (newLastUpdate > latestBannerUpdate) {
						BannerManager bannerManager = new BannerManager();

						bannerManager.DeleteRootBannerFolder();
						PrefUtils.saveBannerLastUpdateTime("" + newLastUpdate);

						// Download each ads
						for (int i = 0; i < banners.Count; i++) {
							DownloadAdImage(i);
						}
					}
					else { // Load current ads
						for (int i = 0; i < banners.Count; i++) {
							SetUpAdView(i);
						}
					}
				},
				(w) => { // On error

				}));
		}

		private void DownloadAdImage(int bannerIndex)
		{
			Banner banner = banners [bannerIndex];
			ApiService service = new ApiService ();

			StartCoroutine (service.DownloadAdImage(this, banner,
				(w) => { // On success
					SetUpAdView(bannerIndex);
				},
				(w) => { // On error

				}));
		}

		private void SetupTopStoryView()
		{
			topStoryImages = new List<Image> ();
			Sprite defaultImageSprite = sampleTopStoryImage.sprite;

			for (int i = 0; i < topStories.Count; i++) {
				Story story = topStories [i];

				Image imageView;
				Button imageButon;

				if (i == 0) {
					imageView = sampleTopStoryImage;
					imageButon = imageView.gameObject.AddComponent<Button> ();

				} else {
					imageView = SetupImageView();
					imageButon = imageView.GetComponent<Button> ();

					imageView.gameObject.SetActive (false);
				}

				topStoryImages.Add (imageView);

				// If story is null, set default image and click function
				if (story == null) {
					imageView.sprite = defaultImageSprite;

					imageButon.onClick.AddListener(() => {
						Router.toDownloadStory();
					});

					continue;
				}

				// If the cover image path of the story is available, set the image to the image view
				if (story.backgroundImagePath != "") {
					imageView.setImagePath (story.backgroundImagePath);  
				} else { // Otherwise, set default image according to the story's theme
					StoryManager sm =  new StoryManager();
					imageView.setImagePath(sm.getPathToThemeImage(story.theme));
				}

				imageButon.onClick.AddListener(() => {
//				GameplayViewController.setSelectedStory(story);
					Router.toGameplay(story);
				});
			}
         
			currentTopStoryIndex = 0;
			UpdateRelatingTextViews();
		}

		void SetUpAdView(int bannerIndex)
		{
			int realIndex = topStories.Count + bannerIndex;
			Banner banner = banners [bannerIndex];
			Image imageView = topStoryImages [realIndex];
			Button imageButton = imageView.GetComponent<Button> ();

			if (!File.Exists(banner.filePath)) {
				DownloadAdImage (bannerIndex);
				return;
			}

			if (banner.filePath != "") {
				imageView.setImagePath (banner.filePath);
			}

			Destroy(imageButton);
			imageView.gameObject.SetActive (false);
		}

		Image SetupImageView()
		{
			Image imageView = Instantiate(sampleTopStoryImage, sampleTopStoryImage.transform.position, sampleTopStoryImage.transform.rotation);
			imageView.transform.SetParent (bannerImagePanel, false);

			return imageView;
		}
		
		void ScrollToImage(int index) {
			animationRunning = true;

			nextTopStoryIndex = index;
			Image currentImage = topStoryImages [currentTopStoryIndex];
			Image nextImage = topStoryImages [index];

			nextImage.gameObject.SetActive (true);
			nextImage.canvasRenderer.SetAlpha( 0.0f );

			currentImage.CrossFadeAlpha (0.0f, SCROLL_ANIMATION_DURATION, false);
			nextImage.CrossFadeAlpha (1.0f, SCROLL_ANIMATION_DURATION, false);

			Invoke ("UpdateOnNextImageDone", SCROLL_ANIMATION_DURATION);
		}

		void ScrollToNextImage()
		{
			if (autoScrollPaused || topStoryImages.Count <= 1) {
				autoScrollPaused = false;
				return;
			}

			nextTopStoryIndex = GetNextStoryIndex ();
			ScrollToImage (nextTopStoryIndex);
		}

		void UpdateOnNextImageDone()
		{
			Image currentImage = topStoryImages [currentTopStoryIndex];
			Image nextImage = topStoryImages [nextTopStoryIndex];

			Debug.Log ("SCROLL - from " + currentTopStoryIndex + " to " + nextTopStoryIndex);

			currentImage.gameObject.SetActive (false);

			currentTopStoryIndex = nextTopStoryIndex;
			animationRunning = false;
		
			UpdateRelatingTextViews ();
		}

		private void UpdateRelatingTextViews()
		{
			if (currentTopStoryIndex < topStories.Count && topStories [currentTopStoryIndex] != null) {
				storyNamePanel.gameObject.SetActive (true);
				storyNameLabel.GetComponent<Text> ().text = topStories [currentTopStoryIndex].name;
			} else {
				storyNamePanel.gameObject.SetActive (false);
			}
		}

		private int GetNextStoryIndex()
		{
			if (currentTopStoryIndex < topStoryImages.Count - 1) {
				return currentTopStoryIndex + 1;
			}

			return 0;
		}
	}
}
