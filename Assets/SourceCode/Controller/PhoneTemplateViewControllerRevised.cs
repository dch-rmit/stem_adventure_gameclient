﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PhoneTemplateViewControllerRevised : MonoBehaviour {

	public Button menuButton;

	public GameObject sideMenuPanel;

	public Button infoButton;
	
	public Canvas Canvas;

	public GameObject infoPanel;

	public Button closesidePanel;

	public GameObject sidePanelGameObject;
	
	// Use this for initialization
	void Start () {
		
		SidePanel();
        
		menuButton.onClick.AddListener(() => {
			sidePanelGameObject.SetActive (true);
		});
		
		closesidePanel.onClick.AddListener(() =>
		{
			sidePanelGameObject.SetActive(false);
		});

	}

	private void SidePanel()
	{ 
		sidePanelGameObject.SetActive(false);
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
