﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Doozy.Engine.Nody;
using Doozy.Engine.UI;
using Proyecto26;
using Script;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using SourceCode.utils;
using TMPro;
//using UnityEngine.Windows;
using Debug = UnityEngine.Debug;

namespace SourceCode.Controller
{

	public class DownloadStoryViewController2 : MonoBehaviour
	{

		public GraphController graphController;
		public GameObject gamePlayView;

		public ScrollRect scrollView;
		public GameObject storyItemPrefab;

		private List<GameObject> storyItemViews;

		private Dictionary<string, string> themeMap;
		private Dictionary<string, List<Story>> allStories;
		private List<Story> currentStoryList;

		private string selectedTheme;

		private DownloadStoryItemViewController downloadStoryItemViewController;

		public GameObject contentPanel;

		// Use this for initialization
		void Start()
		{

//			storyItemViews = new List<GameObject>();
//			allStories = new Dictionary<string, List<Story>>();
//			currentStoryList = new List<Story>();
//
//			LoadAllStories();
		}

		// Called at end of UIView Animation
		public void LoadAllStories()
		{
			
			storyItemViews = new List<GameObject>();
			allStories = new Dictionary<string, List<Story>>();
			currentStoryList = new List<Story>();

			var requestOptions = ApiService.RequestOptions();
			RestClient.Get(requestOptions).Then(response =>
			{
				JSONNode json = JSON.Parse(response.Text);
				JSONArray storiesJson = json.AsArray;
				for (int i = 0; i < storiesJson.Count; i++)
				{
					JSONNode storyJson = storiesJson[i];
					Story story = new Story(storyJson);

					if (story.resourceInfos.Count == 0)
					{
						continue;
					}

					currentStoryList.Add(story);
				}

				UpdateStoryListView();
			}).Catch(err => ConnectionUnavailable(err.Message));
		}

		void ConnectionUnavailable(string message)
		{
			//tempStorySelectionItem.SetActive(false);
			Debug.Log("A download error occurred.");
			UIPopup uiPopup = UIPopup.GetPopup("InternetUnavailable");
			uiPopup.Show();
		} 

		private bool CachePictures(int storyID)
		{
			return File.Exists(Application.persistentDataPath + "/Script/" + storyID + "/" + "cover.png");
		}




		private void UpdateStoryListView()
		{

			//Destroy all children so that views aren't duplicated in list.
			foreach (Transform child in contentPanel.transform)
			{
				Destroy(child.gameObject);
			}
			
			foreach (Story story in currentStoryList)
			{
				SetupStoryItemView(story);
			}

			foreach (var story in GetOrphanedStories())
			{
				SetupOrphanedStoryItemView(story);
			}
		}

		private void SetupOrphanedStoryItemView(Story story)
		{
			GameObject storyItemView = Instantiate(storyItemPrefab,
				storyItemPrefab.transform.position, storyItemPrefab.transform.rotation);

			Button buttonComponent = storyItemView.GetComponent<Button>();
			buttonComponent.enabled = false;

			storyItemView.transform.Find("StoryTitle").GetComponent<TextMeshProUGUI>().text = story.name;
			storyItemView.transform.Find("StoryDescription").GetComponent<TextMeshProUGUI>().text =
				"By " + story.authorName + "\nVersion " + story.version;

			if (story.backgroundImagePath != null)
			{
				Image image = storyItemView.transform.Find("StoryImage").GetComponent<Image>();
				image.preserveAspect = true;
				image.setImagePath(story.backgroundImagePath);
			}

			storyItemView.transform.SetParent(scrollView.content, false);

			// Setup the download & remove button
			Button removeButton = storyItemView.transform.Find("RemoveButton").GetComponent<Button>();
			removeButton.gameObject.SetActive(true);

			// Story not available to download. Hide button. 
			Button downloadButton = storyItemView.transform.Find("DownloadButton").GetComponent<Button>();
			downloadButton.gameObject.SetActive(false);

			removeButton.onClick.AddListener(() =>
			{
				MessageDialogViewController.show(
					"Do you want to remove this story?", true,
					() =>
					{
						StoryManager sm = new StoryManager();
						sm.removeStory(story.id);

						buttonComponent.enabled = false;
						removeButton.gameObject.SetActive(false);

						Destroy(storyItemView.gameObject);
						MessageDialogViewController.dismiss();
					});
			});
			StoryButtonPlayListener(buttonComponent, story.id);

			storyItemViews.Add(storyItemView);
		}

		// Returns a list of stories that have been downloaded but not are not available to be downloaded. 
		private IEnumerable<Story> GetOrphanedStories()
		{
			//Get downloaded stories
			StoryManager storyManager = new StoryManager();
			List<Story> downloadedStories = storyManager.GetAllStories();

			//Get all story ids for downloaded stories.
			List<int> storyIds = currentStoryList.Select(story => story.id).ToList();

			// If story is downloaded but not available as a download anymore add it to the current story list.
			return downloadedStories.Where(story => !storyIds.Contains(story.id)).ToList();
		}

		// Sets up the view for each story that is available to be downloaded through the API.
		private void SetupStoryItemView(Story story)
		{

			GameObject storyItemView = Instantiate(storyItemPrefab,
				storyItemPrefab.transform.position, storyItemPrefab.transform.rotation);

			StoryComponents storyComponents = new StoryComponents(storyItemView, story);

			storyItemView.transform.SetParent(scrollView.content, false);

			Image image = storyItemView.transform.Find("StoryImage").GetComponent<Image>();

			SetUrlForImageComponent(image, story);

			string lastUpdateTime = PrefUtils.loadDownloadedStoryLastUpdateTime(story.id);

			DebugStoryUpdateMessage(story, lastUpdateTime);

			// 1527793830 - 1527793830
			if (lastUpdateTime.Length > 0)
			{
				// This story was already downloaded

				if (lastUpdateTime == story.lastUpdate)
				{
					// The current version is up-to-date
					StoryUpToDate(storyComponents);
				}
				else
				{
					// New version available
					NewStoryAvailable(storyComponents);
				}

				storyComponents.RemoveButton.gameObject.SetActive(true);

			}
			else
			{
				// This story is not downloaded yet
				storyComponents.SetStoryAvailable();
			}

			// Set click/touch listeners for the item view.
			//Note: Currently, cannot be refactored to DownloadStoryItemViewController which is where it should live.
			DownloadButtonListener(story, storyComponents.DownloadButton, storyComponents.DownloadButtonText,
				storyComponents.ButtonComponent, storyComponents.RemoveButton);
			RemoveButtonListener(story, storyComponents.RemoveButton, storyComponents.ButtonComponent,
				storyComponents.DownloadButtonText, storyComponents.DownloadButton);
			StoryButtonPlayListener(storyComponents.ButtonComponent, story.id);

			storyItemViews.Add(storyItemView);
		}


		// StoryUpToDate set the state of the download button when there are no updates available for a story.
		private void StoryUpToDate(StoryComponents storyComponents)
		{
			storyComponents.DownloadButtonText.text = Constants.DownloadButtonSetDownloaded;
			storyComponents.DownloadButton.enabled = false;
			storyComponents.ButtonComponent.enabled = true;
		}

		// NewStoryAvailable sets the state of the download button when the story has been updated.
		private void NewStoryAvailable(StoryComponents storyComponents)
		{
			storyComponents.DownloadButtonText.text = Constants.DownloadButtonSetUpdate;
		}

		public void SetUrlForImageComponent(Image image, Story story)
		{
			string path = Application.persistentDataPath + "/Script/" + story.id + "/";
			Debug.Log("Background image path is: " + path);
			string jpgPath = path + "cover.jpg";
			string pngPath = path + "cover.png";
			string JPGPath = path + "cover.JPG";
			bool jpgExsts = ImageExtensions.GetImagePath(jpgPath);
			Debug.Log("jpg exists equals " + jpgExsts + " " + jpgPath);
			bool pngExists = ImageExtensions.GetImagePath(pngPath);
			Debug.Log("png exists equals " + pngExists + " " + pngPath);
			bool JPGExists = ImageExtensions.GetImagePath(JPGPath);
			Debug.Log("jpg exists equals " + JPGExists + " " + JPGPath);

			if (jpgExsts)
			{
				image.setImagePath(jpgPath);
				Debug.Log("Image loaded from " + jpgPath);
				return;
			}

			if (pngExists)
			{
				image.setImagePath(pngPath);
				Debug.Log("Image loaded from " + pngPath);
				return;
			}

			if (JPGExists)
			{
				image.setImagePath(JPGPath);
				Debug.Log("Image loaded from " + JPGPath);
				return;
			}
			
			StartCoroutine(
				image.setImageURL(ApiRoute.CoverImageUrl(story.id, StoryManager.CoverImageExtension(story)))
			);
		}

		// StoryComponents encapsulates the Components held in a StoryItemView.
		private static void DebugStoryUpdateMessage(Story story, string lastUpdateTime)
		{
			Debug.Log("Story ID: " + story.id);
			Debug.Log("Loaded lastUpdateTime: " + lastUpdateTime);
			Debug.Log("Last Update: " + story.lastUpdate);
		}

		private void DownloadButtonListener(Story story, Button downloadButton, TextMeshProUGUI downloadButtonText,
			Button buttonComponent,
			Button removeButton)
		{
			downloadButton.onClick.AddListener(() =>
			{
//			Debug.Log("****** Button Clicked ****** Story Name: " + story.name);

				downloadButtonText.text = "Downloading..";
				downloadButton.enabled = false;

				ApiService service = new ApiService();

				service.DownloadStory(this, story,
					(w) =>
					{
						// On success
						downloadButtonText.text = Constants.DownloadButtonSetDownloaded;
						buttonComponent.enabled = true;
						removeButton.gameObject.SetActive(true);
					},
					(w) =>
					{
						// On error
						downloadButtonText.text = Constants.DownloadButton;
						downloadButton.enabled = true;
					});
			});
		}

		private void RemoveButtonListener(Story story, Button removeButton, Button buttonComponent,
			TextMeshProUGUI downloadButtonText,
			Button downloadButton)
		{
			removeButton.onClick.AddListener(() =>
			{
				MessageDialogViewController.show(
					"Do you want to remove this story?", true,
					() =>
					{
						StoryManager sm = new StoryManager();
						sm.removeStory(story.id);

						buttonComponent.enabled = false;
						removeButton.gameObject.SetActive(false);

						downloadButtonText.text = Constants.DownloadButton;
						downloadButton.enabled = true;

						MessageDialogViewController.dismiss();
					});
			});
		}

		private void StoryButtonPlayListener(Button buttonComponent, int storyId)
		{
			buttonComponent.onClick.AddListener(() =>
			{
				Story selectedStory = new StoryManager().getStoryById(storyId);

				GameManager.GetInstance().SetSelectedStory(selectedStory);
				GameplayUIUpdater updater = gamePlayView.GetComponent<GameplayUIUpdater>();
				updater.SetUpNewGameUI(GameManager.GetInstance().CurrentGame);
				graphController.GoToNodeByName("Play");
			});
		}

		private class StoryComponents
		{
			public Button RemoveButton { get; }
			public Button DownloadButton { get; }
			public TextMeshProUGUI DownloadButtonText { get; }

			public Button ButtonComponent { get; }
			public Image Image { get; }


			public StoryComponents(GameObject storyItemView, Story story)
			{
				// Get all the different components that form the StoryItemView
				RemoveButton = storyItemView.transform.Find("RemoveButton").GetComponent<Button>();
				DownloadButton = storyItemView.transform.Find("DownloadButton").GetComponent<Button>();
				DownloadButtonText = DownloadButton.transform.Find("Text").GetComponent<TextMeshProUGUI>();
				ButtonComponent = storyItemView.GetComponent<Button>();
				Image = storyItemView.transform.Find("StoryImage").GetComponent<Image>();

				SetStoryItemAttributeViews(storyItemView, story);
				// Button starts disabled.
				ButtonComponent.enabled = false;
				// Remove button starts disabled until can confirm story downloaded.
				RemoveButton.gameObject.SetActive(false);

				// Set aspect for item image.
				PreserveImageAspect(true);
			}

			// Initialize the Name and authorship of the story.
			private void SetStoryItemAttributeViews(GameObject storyItemView, Story story)
			{
				storyItemView.transform.Find("StoryTitle").GetComponent<TextMeshProUGUI>().text = story.name;
				storyItemView.transform.Find("StoryDescription").GetComponent<TextMeshProUGUI>().text =
					"By " + story.authorName + "\nVersion " + story.version;
			}

			public void PreserveImageAspect(bool b)
			{
				Image.preserveAspect = b;
			}

			public void SetStoryAvailable()
			{
				DownloadButtonText.text = Constants.DownloadButton;
			}
		}
	}
}
