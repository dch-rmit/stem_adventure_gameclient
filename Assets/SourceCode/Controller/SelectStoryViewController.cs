﻿using System.Collections.Generic;
using Script;
using SourceCode.Controller.PopupTest;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SourceCode.Controller
{
	/// <summary>
	/// SelectStoryViewController is the controller for the view that allows a user to select a story to play from
	/// the list of downloaded stories. 
	/// </summary>
	public class SelectStoryViewController : MonoBehaviour {
		public ScrollRect scrollView;
		public GameObject tempStorySelectionItem;

		private List<Story> stories;
		private List<GameObject> storyItemViews;

		[SerializeField]public GameObject gamePlayView;
	
		
		
		public void LoadStory()
		{
			StoryManager storyManager = new StoryManager ();
			
			List<Story> allStories = storyManager.GetAllStories();
			stories = new List<Story>();

			foreach (Story s in allStories)
			{
				stories.Add(s);
			}
			
			UpdateStoryListView ();
		}
     
		private void UpdateStoryListView()
		{
			foreach (var storyItemView in storyItemViews)
			{
				Destroy(storyItemView);
			}
			foreach (Story story in stories) {
				
				string imagePath = story.backgroundImagePath;

				//Set up the storyItemView with the tempStorySelectionItem as a placeholder.
				GameObject storyItemView = Instantiate(tempStorySelectionItem, 
					tempStorySelectionItem.transform.position, tempStorySelectionItem.transform.rotation);
				storyItemViews.Add(storyItemView);

				Image imageView = storyItemView.transform.Find ("StoryImage").GetComponent<Image> ();
				
				storyItemView.transform.Find("StoryTitle").GetComponent<TextMeshProUGUI>().text = story.name;
				storyItemView.transform.Find("StoryDescription").GetComponent<TextMeshProUGUI>().text = story.keyLearning;
				storyItemView.transform.Find("StoryTheme").GetComponent<Text>().text = story.theme;


				int userId = PrefUtils.loadUserId ();
				int marks = PrefUtils.loadFullMarkStoryAttempt (story.id, userId);
				bool badgeShown = marks > 0;
				storyItemView.transform.Find("Badge").gameObject.SetActive(badgeShown);

				// If the cover image path of the story is available, set the image to the image view
				if (!string.IsNullOrEmpty(imagePath)) {
					imageView.setImagePath (imagePath);
				} else { // Otherwise, set default image according to the story's theme
					StoryManager sm =  new StoryManager();
					imageView.setImagePath(sm.getPathToThemeImage(story.theme));
				}

				imageView.preserveAspect = true;

				storyItemView.transform.Find ("StoryScore").gameObject.SetActive (false);

				storyItemView.transform.SetParent(scrollView.content, false);

				storyItemView.transform.GetComponent<Button>().onClick.AddListener(() => {
					ToPlayGame(story);
				});
			}
		}

		/// <summary>
		/// Set the Current Game to the One selected. Scene transition handled by DoozyUI.
		/// </summary>
		/// <param name="story"></param>
		private void ToPlayGame(Story story = null)
		{
			if (story is null) return;
			// Pass this story to the Game Manager to initialize a new Game.
			GameManager.GetInstance().StartNewGame(story);
			GameplayUIUpdater updater = gamePlayView.GetComponent<GameplayUIUpdater>();
			updater.SetUpNewGameUI(GameManager.GetInstance().CurrentGame);

			//TODO Remove this once we have decided on a theme.
			GameplayViewControllerPopup.SetSelectedStory(story);
		}

		private void Start()
		{
			storyItemViews = new List<GameObject>();
		}
	}
}
