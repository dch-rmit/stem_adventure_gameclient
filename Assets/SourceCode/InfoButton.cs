﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class InfoButton : MonoBehaviour
{

    public Button infoButton;

    public GameObject infoPanel;

    public Button closeButton;

    // Start is called before the first frame update
    void Start()
    {
        infoButton.onClick.AddListener(InfoButtonClicked);
        infoPanel.SetActive(false);
        closeButton.onClick.AddListener(CloseButtonClicked);

        
    }
    
    private void InfoButtonClicked()
    {
        infoPanel.SetActive(true);
    }

    private void CloseButtonClicked()
    {
        infoPanel.SetActive(false);
    }

}
