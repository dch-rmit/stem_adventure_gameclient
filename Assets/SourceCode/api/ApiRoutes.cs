﻿using System;
using SourceCode.utils;

public class ApiRoute
{
	public static string KEY_STORY_ID = "story_id";
	public static string KEY_ID = "id";
	public static string KEY_N = "n"; // name
	public static string KEY_E = "e"; // 
	public static string KEY_D = "d"; // description
	public static string KEY_A = "a"; // author
	public static string KEY_T = "t"; // theme
	public static string KEY_V = "v"; // version
	public static string KEY_SID = "sid";
	public static string KEY_P = "p"; // 
	public static string KEY_LU = "lu"; // last updated
	public static string KEY_R = "r"; // Resources
	public static string KEY_FN = "fn"; // Filename
	public static string KEY_FT = "ft"; // Filetype
	public const string KEY_AUTHOR_NAME = "author_name"; //Overriden author name.

	public static string KEY_NAME = "name";
	public static string KEY_STUDENT_ID = "student_id";

	public static string KEY_SESSION = "session";
	public static string KEY_EMAIL = "email";
	public static string KEY_PASSWORD = "password";
	public static string KEY_STORIES = "stories";
	public static string KEY_BANNERS = "banners";

	public static string KEY_MARKS = "marks";
	public static string KEY_WAC = "wac"; // wrong answer count
	public static string KEY_ERRORS = "errors";

//	public static string ROOT_URL = "http://localhost:3000/";
	public static string ROOT_URL = "https://stemadventures.stem-adv.cloud.edu.au/";


	public static string LOGIN_URL = ROOT_URL + "users/login"; // Post, Params: email, password
	public static string SESSION_URL = ROOT_URL + "users/session"; // Post, Params: id, session

	public static string ALL_STORIES_URL = ROOT_URL + "stories/index"; // Get
	public static string DOWNLOAD_STORY_URL = ROOT_URL + "stories/download"; // Get. Append story ID to the URL to download it
	public static string SUBMIT_ATTEMPT_URL = ROOT_URL + "stories/submit"; // Post

	public static string ALL_ADS_URL = ROOT_URL + "banner/index"; // Get
	public static string DOWNLOAD_ADS_URL = ROOT_URL + "banner/download?"; // Get. Append file name & type to the URL to download it

	public static string SIGNUP_PAGE_URL = ROOT_URL + "pages/signup";
	

	public ApiRoute ()
	{
	}

	// Returns the route to request a cover image for a story.
	public static string CoverImageUrl(int id, string coverImageExt)
	{
		return DOWNLOAD_STORY_URL + "?id=" + id + "&name=cover&type=" + coverImageExt;
	}


}

