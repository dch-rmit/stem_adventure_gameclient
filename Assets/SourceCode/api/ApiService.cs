﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Proyecto26;
using SourceCode.utils;


/// <summary>
///  APIService handles the networking logic for login and downloading resources.
/// </summary>
public class ApiService
{
	public IEnumerator CheckSession(string userId, string session, OnSuccess onSuccess, OnError onError)
	{

		WWWForm form = new WWWForm();
		form.AddField("id", userId);
		form.AddField("session", session);

		using (var w = UnityWebRequest.Post(ApiRoute.SESSION_URL, form))
		{
			yield return w.SendWebRequest();

			if (w.isNetworkError || w.isHttpError) { // Error
				Debug.Log("ERROR: " + w.error);
					onError?.Invoke (w);
			}
			else { // Success
				Debug.Log("DONE: " + w.downloadHandler.text);
                onSuccess?.Invoke (w);
			}
		}
	}

	public IEnumerator HandleLogin(string email, string password, OnSuccess onSuccess, OnError onError)
	{
        WWWForm form = new WWWForm();
		form.AddField(ApiRoute.KEY_EMAIL, email);
		form.AddField(ApiRoute.KEY_PASSWORD, password);

		using (var w = UnityWebRequest.Post(ApiRoute.LOGIN_URL, form))
		{
			yield return w.SendWebRequest();

			if (w.isNetworkError || w.isHttpError) { // Error
				Debug.Log("ERROR: " + w.error);
                onError?.Invoke (w);
            }
			else { // Success
				Debug.Log("DONE: " + w.downloadHandler.text);
                onSuccess?.Invoke (w);
            }
		}
	}

	public void DownloadStory(MonoBehaviour viewController, Story story, OnSuccess onSuccess, OnError onError)
	{
		List<Story.ResourceInfo> resInfos = story.resourceInfos;
		Queue<Story.ResourceInfo> resQueue = new Queue<Story.ResourceInfo> ();

		// Queue up the list of resources
		foreach (Story.ResourceInfo rInfo in resInfos) {
			resQueue.Enqueue (rInfo);
		}

		// Start downloading
		Story.ResourceInfo nextRes = resQueue.Dequeue ();
		
		viewController.StartCoroutine (
			DownloadResource (viewController, story, nextRes, resQueue, onSuccess, onError));
	}

	public IEnumerator DownloadResource(MonoBehaviour viewController, Story story, Story.ResourceInfo resInfo, 
		Queue<Story.ResourceInfo> resQueue, 
		OnSuccess onSuccess, OnError onError)
	{
		string url = ApiRoute.DOWNLOAD_STORY_URL 
			+ "?id=" + story.id + "&name=" + resInfo.fileName + "&type=" + resInfo.fileType;

		url = url.Replace(" ", "%20");

		Debug.Log("Download URL: " + url);

		using (var w = UnityWebRequest.Get(url))
		{
			yield return w.SendWebRequest();

			if (w.isNetworkError || w.isHttpError) { // Error
				Debug.Log("downloadResource - ERROR: " + w.error);

                onError?.Invoke (w);
			}
			else { // Success
				Debug.Log("downloadResource - DONE: " + w.downloadHandler.text);

				var storyManager = new StoryManager ();
				storyManager.SaveDownloadedResource (w, story, resInfo);
 
				// If all files have been downloaded,
				// save those files into correct location
				if (resQueue.Count == 0) {
					storyManager.SaveDownloadedStory (story);
                    onSuccess?.Invoke (w);
				} else { // Otherwise
					Story.ResourceInfo nextRes = resQueue.Dequeue ();
					viewController.StartCoroutine (
						DownloadResource (viewController, story, nextRes, resQueue, onSuccess, onError));
				}
			}
		}
	}

	public IEnumerator DownloadAdImage(MonoBehaviour viewController, Banner banner, 
		OnSuccess onSuccess, OnError onError)
	{
		string url = ApiRoute.DOWNLOAD_ADS_URL 
			+ "?name=" + banner.fileName + "&type=" + banner.fileType;

		Debug.Log("Download URL: " + url);

		using (var w = UnityWebRequest.Get(url))
		{
			yield return w.SendWebRequest();

			if (w.isNetworkError || w.isHttpError) { // Error
				Debug.Log("download Ad - ERROR: " + w.error);
                onError?.Invoke (w);
			}
			else { // Success
				Debug.Log("download Ad - DONE: " + w.downloadHandler.text);

				var bannerManager = new BannerManager ();
				bannerManager.SaveDownloadedAdImage (w, banner);

                onSuccess?.Invoke (w);
			}
		}
	}

	public IEnumerator DoGetMethod(string url, OnSuccess onSuccess, OnError onError)
	{
		Debug.Log(url);
		using (var w = UnityWebRequest.Get(url))
		{
			
			w.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
			w.SetRequestHeader("Content-Type", "application/json");
			yield return w.SendWebRequest();

			if (w.isNetworkError || w.isHttpError) { // Error
				Debug.Log("ERROR: " + w.error);
                onError?.Invoke (w);
			}
			else { // Success
				Debug.Log("DONE: " + w.downloadHandler.text);
                Debug.Log(url);
                onSuccess?.Invoke (w);
			}
		}
	}

	public IEnumerator SubmitAttempRecord(int storyId, int userId, int marks, int wac, OnSuccess onSuccess, OnError onError)
	{
		WWWForm form = new WWWForm();
		form.AddField(ApiRoute.KEY_STORY_ID, storyId);
		form.AddField(ApiRoute.KEY_ID, userId);
		form.AddField(ApiRoute.KEY_MARKS, marks);
		form.AddField(ApiRoute.KEY_WAC, wac);
         
		using (var w = UnityWebRequest.Post(ApiRoute.SUBMIT_ATTEMPT_URL, form))
		{
			yield return w.SendWebRequest();

			if (w.isNetworkError || w.isHttpError) { // Error
				Debug.Log("ERROR: " + w.error);
                onError?.Invoke (w);
			}

			else { // Success
				Debug.Log("DONE: " + w.downloadHandler.text);
                onSuccess?.Invoke (w);
			}
		}
	}
	
	public static RequestHelper RequestOptions()
	{
		var requestOptions = new RequestHelper
		{
			Uri = ApiRoute.ALL_STORIES_URL,
			ContentType = "application/json",
			// Set headers otherwise UnityWebRequest will cause server to return a "406" error.
			Headers = new Dictionary<string, string>
			{
				{"Accept-Encoding", "gzip, deflate"},
				{"Accept", "*/*"},
			},
		};
		return requestOptions;
	}
	
	public static RequestHelper BannerRequestOptions()
	{
		var requestOptions = new RequestHelper
		{
			Uri = ApiRoute.ALL_ADS_URL,
			ContentType = "application/json",
			// Set headers otherwise UnityWebRequest will cause server to return a "406" error.
			Headers = new Dictionary<string, string>
			{
				{"Accept-Encoding", "gzip, deflate"},
				{"Accept", "*/*"},
			},
		};
		return requestOptions;
	}


	public delegate void OnSuccess(UnityWebRequest w);
	public delegate void OnError(UnityWebRequest w);
}

