﻿using System.IO;
using System.Collections.Generic;
using SourceCode.utils;
using UnityEngine;
using UnityEngine.Networking;

public class StoryManager
{
	public const string ThemeScience = "Science";
	public const string ThemeTechnology = "Technology";
	public const string ThemeEngineering = "Engineering";
	public const string ThemeMath = "Mathematics";

	public const string ROOT_STORY_FOLDER_NAME = "/Script";

	private const int TOP_STORIES_COUNT = 3;

	public StoryManager ()
	{
	}

	public Dictionary<string, string> CreateThemeMap() {
        var themeMap = new Dictionary<string, string>
        {
            { ThemeScience, ThemeScience },
            { ThemeTechnology, ThemeTechnology },
            { ThemeEngineering, ThemeEngineering },
            { ThemeMath, ThemeMath },

            // Other themes will be mapped to Science by default
            { "Chemistry", ThemeScience },
            { "Biology", ThemeScience },
            { "Geography", ThemeScience },
            { "Chemistry Adventures", ThemeScience },
            { "Biology Adventures", ThemeScience },
            { "Geography Adventures", ThemeScience }
        };

        return themeMap;
	}

	public List<Story> GetTopStoriesFromStoryList()
	{
		List<Story> allStories = GetAllStories ();
		List<Story> topStories = new List<Story> ();

		for (int i = 0; i < TOP_STORIES_COUNT; i++) {
			if (allStories.Count == 0) {
				break;
			}

			var random = new System.Random();
			int randomIndex = random.Next(0, allStories.Count);

			topStories.Add (allStories[randomIndex]);
			Debug.Log ("Get Top - Background Image Path: " + allStories[randomIndex].backgroundImagePath);
			allStories.RemoveAt (randomIndex);
		}

		topStories.Add (null);
		return topStories;
	}

	public bool saveDownloadedStory(UnityWebRequest w, Story story)
	{
		string saveToFolderPath = getRootStoryFolderPath () + "/" + story.id;
		string filePath = saveToFolderPath + "/" + story.id + ".plist";

		// Create the folder for this story if not existed
		if (!Directory.Exists (saveToFolderPath)) {
			Directory.CreateDirectory (saveToFolderPath);
		}

		// Save downloaded story ID
		PrefUtils.saveDownloadedStoryLastUpdateTime(story.id, story.lastUpdate);
		PrefUtils.save ();

		// Delete old version if existed
		if (File.Exists(filePath)) {
			File.Delete (filePath);
		}

		return FileUtils.byteArrayToFile (filePath, w.downloadHandler.data);
	}

	public void SaveDownloadedStory(Story story)
	{
		// Save downloaded story ID
		PrefUtils.saveDownloadedStoryLastUpdateTime(story.id, story.lastUpdate);
		PrefUtils.save ();
	}

	public bool SaveDownloadedResource(UnityWebRequest w, Story story, Story.ResourceInfo resInfo)
	{
		string saveToFolderPath = getRootStoryFolderPath () + "/" + story.id;
		string fileName = resInfo.fileType == "plist" ? story.id + "" : resInfo.fileName;
		string filePath = saveToFolderPath + "/" + fileName + "." + resInfo.fileType;

		Debug.Log("Save file path: " + filePath);

		// Create the folder for this story if not existed
		if (!Directory.Exists (saveToFolderPath)) {
			Directory.CreateDirectory (saveToFolderPath);
		}

		// Delete old version if existed
		if (File.Exists(filePath)) {
			File.Delete (filePath);
		}

		return FileUtils.byteArrayToFile (filePath, w.downloadHandler.data);
	}

	public Story parseStoryFromFolder(DirectoryInfo storyFolder)
	{
		FileInfo[] files = storyFolder.GetFiles ("*.plist");

        
		// Not exactly 1 plist in the folder of a story => invalid
		if (files.Length != 1)
		{
			return null;
		}

		string storyFilePath = files[0].FullName;
		PlistParser plistParser = new PlistParser ();

        Debug.Log(storyFilePath);
        Debug.Log(storyFolder);
        return plistParser.ParseStory (storyFilePath, storyFolder);
	}

	public List<Story> GetAllStories()
	{
		DirectoryInfo[] directories = getAllStoryFolders ();
		List<Story> stories = new List<Story>();

		foreach (DirectoryInfo dir in directories)
		{
			Story story = parseStoryFromFolder (dir);

			// Add the story to the list if it's parsed successfully
			if (story != null)
			{
				stories.Add (story);
			}
		}

		return stories;
	}

	public Story getStoryById(int storyId)
	{
		string storyFolderPath = getRootStoryFolderPath () + "/" + storyId;
		DirectoryInfo dir = new DirectoryInfo (storyFolderPath);
		return parseStoryFromFolder (dir);
	}

	public List<Story> getStoriesByTheme(string theme)
	{
		return getStoriesByTheme (theme, GetAllStories ());
	}

	public List<Story> getStoriesByTheme(string theme, List<Story> allStories)
	{
		Dictionary<string, string>  themeMap = CreateThemeMap ();
		List<Story> resultedStories = new List<Story> ();

		foreach (Story s in allStories) {
			string realTheme = StoryManager.ThemeScience;

			if (themeMap.ContainsKey (s.theme)) {
				realTheme = themeMap[s.theme];
			}

			Debug.Log ("Find: '" + s.theme + "' - Real: '" + realTheme + "'");

			if (realTheme == theme) {
				resultedStories.Add (s);
			}
		}

		return resultedStories;
	}

	public DirectoryInfo[] getAllStoryFolders()
	{
		DirectoryInfo rootStoryDi = getRootStoryFolder ();

		// TODO: Maybe the list of directories should be validated before returning,
		// i.e. check if a folder contains a plist file.
		return rootStoryDi.GetDirectories ();
	}

	public void removeStory(int storyId)
	{
		string storyFolderPath = getRootStoryFolderPath () + "/" + storyId;

		if (Directory.Exists(storyFolderPath)) {
			Directory.Delete (storyFolderPath, true);
		}

		PrefUtils.saveDownloadedStoryLastUpdateTime (storyId, "");
	}

	public DirectoryInfo getRootStoryFolder()
	{
		string rootStoryFolderPath = getRootStoryFolderPath ();
		DirectoryInfo di;

		// Create the all stories folder if not existed
		if (!Directory.Exists (rootStoryFolderPath)) {
			di = Directory.CreateDirectory (rootStoryFolderPath);
		} else {
			di = new DirectoryInfo (rootStoryFolderPath);
		}

		return di;
	}

	public string getRootStoryFolderPath()
	{
		return Application.persistentDataPath + ROOT_STORY_FOLDER_NAME;
	}

	public Sprite getThemeSprite(string theme)
	{
		return Resources.Load<Sprite>("Assets/Resource/default_" + getPathToThemeImage(theme).ToLower() + "");
	}

	public string getPathToThemeImage(string theme)
	{
		string realTheme = getRealTheme(theme);
		return "Assets/Resource/default_" + realTheme.ToLower () + ".png";
	}

	public string getRealTheme(string theme)
	{
		Dictionary<string, string> themeMap = CreateThemeMap ();

		if (themeMap.ContainsKey(theme)) {
			return themeMap [theme];
		} else {
			return ThemeScience;
		}
	}
	// Returns the extension used for the cover image for a story.
	public static string CoverImageExtension(Story story)
	{
		string coverImageExt = "";

		// Find the extension of the cover image
		foreach (Story.ResourceInfo res in story.resourceInfos) {
			if (res.fileName != "cover") continue;
			coverImageExt = res.fileType;
			break;
		}
		return coverImageExt;
	}
}

