﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class GameObjectExtensions
{

	public static void setWidth(this GameObject gameObject, float width)
	{
		RectTransform rectTransform = gameObject.GetComponent<RectTransform> ();
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
	}

	public static void setHeight(this GameObject gameObject, float height)
	{
		RectTransform rectTransform = gameObject.GetComponent<RectTransform> ();
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
	}

	public static float getWidth(this GameObject gameObject)
	{
		RectTransform rectTransform = gameObject.GetComponent<RectTransform> ();
		return rectTransform.rect.width;
	}

	public static float getHeight(this GameObject gameObject)
	{
		RectTransform rectTransform = gameObject.GetComponent<RectTransform> ();
		return rectTransform.rect.height;
	}
}

