﻿namespace SourceCode.utils
{
    //This class holds static strings to be used in the GUI.
    public static class Constants
    {
        // Different strings for state of download button.
        public const string DownloadButton = "Download";
        public const string DownloadButtonSetUpdate = "Update";
        public const string DownloadButtonSetDownloaded = "Downloaded";
        
        //Default extension for cover image for stories
        public const string CoverImageExtension = "png";
    }
}