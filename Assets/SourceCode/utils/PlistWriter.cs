﻿using System;
using System.Collections;
using System.Collections.Generic;

public class PlistWriter
{
	public PlistWriter ()
	{
		
	}

	public void writeStoryToFilePath(Story story, string filePath)
	{
		PlistDocument plist = new PlistDocument ();

		plist.Create ();

		// Add story name, background image path, preface, and starting stage ID
		plist.root.SetString (PlistStoryKey.KEY_STORY_NAME, story.name);
		plist.root.SetString (PlistStoryKey.KEY_BACKGROUND_IMAGE_PATH, story.backgroundImagePath);
		plist.root.SetString (PlistStoryKey.KEY_PREFACE, story.preface);
		plist.root.SetString (PlistStoryKey.KEY_START_STAGE_ID, story.startStageId);


		PlistElementDict stages = plist.root.CreateDict (PlistStoryKey.KEY_STAGES);

		foreach (KeyValuePair<string, Stage> entry in story.stages) {
			Stage stage = entry.Value;
			PlistElementDict plistStage = stages.CreateDict (entry.Key);
			PlistElementArray images = plistStage.CreateArray (PlistStoryKey.KEY_IMAGES);
			PlistElementArray selections = plistStage.CreateArray (PlistStoryKey.KEY_SELECTIONS);

			plistStage.SetString (PlistStoryKey.KEY_STAGE_NAME, stage.name);
			plistStage.SetString (PlistStoryKey.KEY_DESCRIPTION, stage.description);
//			plistStage.SetInteger (PlistStoryKey.KEY_MARKS, stage.marks);

			// Add images
			foreach (Stage.Image image in stage.images) {
				PlistElementDict plistImage = images.AddDict();

				plistImage.SetString (PlistStoryKey.KEY_IMAGE_PATH, image.imagePath);
				plistImage.SetInteger (PlistStoryKey.KEY_X, image.x);
				plistImage.SetInteger (PlistStoryKey.KEY_Y, image.y);
			}

			// Add selections
			foreach (StageSelection seletion in stage.selections) {
				PlistElementDict plistSelection = selections.AddDict();

				plistSelection.SetString (PlistStoryKey.KEY_SELECTION_TEXT, seletion.text);
				plistSelection.SetString (PlistStoryKey.KEY_NEXT_STAGE_ID, seletion.nextStageId);
				plistSelection.SetInteger (PlistStoryKey.KEY_MARKS, seletion.marks);
			}
		}

		plist.WriteToFile (filePath);
	}
}

