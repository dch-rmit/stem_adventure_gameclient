﻿using System;
using System.Linq;

namespace SourceCode.utils
{
    /// <summary>
    /// This class contains various string manipulation methods used to adjust text displayed in the UI such as
    /// message boxes and buttons.
    /// </summary>
    public static class UITextUtils
    {
        /// <summary>
        /// Take a collection of strings and return the longest line length.
        /// </summary>
        /// <param name="lineOfText"></param>
        /// <returns></returns>
        public static int LongestLineLength(string[] lineOfText)
        {
            return lineOfText.Select(t => t.Length).Concat(new[] {0}).Max();
        }

        /// <summary>
        /// Take the options from a game stage and return the longest line length.
        /// </summary>
        /// <param name="selections"></param>
        /// <returns>Longest line length from selections.</returns>
        public static int FindLongestStringinSelectionArray(StageSelection[] selections)
        {
            string[] buttonText = ExtractTextFromSelectionArray(selections);
            return LongestLineLength(buttonText);
        }

        private static string[] ExtractTextFromSelectionArray(StageSelection[] selections)
        {
            string[] buttonText = new string[selections.Length];
            for (int i=0; i< selections.Length; i++)
            {
                buttonText[i] = selections[i].text;
            }

            return buttonText;
        }

        /// <summary>
        /// Counts the number of lines in a string using "\n" as delimiter.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static int CountLines(string text)
        {
            return text.Split('\n').Length;
        }

        public static string CutLongButtonTextIntoLines(string text, int maxLineLength)
        {
            string resultedText = "";
            string lastLine = text;
            // Cut the text line by line
            while (lastLine.Length > maxLineLength)
            {
                // Find last space for current line
                int j = maxLineLength;
                while (lastLine.ToCharArray()[j] != ' ' && j > 0)
                {
                    j--;
                }

                // If there's no space, cut the line at index MAX_SELECTION_TEXT_LENGTH
                if (j == 0)
                {
                    j = maxLineLength;
                }

                resultedText += lastLine.Substring(0, j) + "\n";
                int cutIndex = lastLine.ToCharArray()[j] == ' ' ? j + 1 : j;
                lastLine = lastLine.Substring(cutIndex);
            }

            text = resultedText + lastLine;
            return text;
        }

        public static string AddSpaceToButtonString(int minimumTextLineLength, string text)
        {
            int halfMissingLength = (int) Math.Ceiling (((float) (minimumTextLineLength - text.Length)) / 2);
            string spaces = new string (' ', halfMissingLength);
            text += spaces;
            return text;
        }

        public static string AdjustButtonStringForTextLength(string text, int minimumTextLenth, int maximumTextLength, int longestLineLength)
        {

            if (longestLineLength < minimumTextLenth)
            {
                return UITextUtils.AddSpaceToButtonString(minimumTextLenth, text);
            }

            if (text.Length > maximumTextLength)
            {
                return UITextUtils.CutLongButtonTextIntoLines(text, maximumTextLength);
            }
            return text;
        }
    }
}