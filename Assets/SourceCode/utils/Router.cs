﻿using System;
using SourceCode.Controller;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Router
{
	public static string toSignUp()
	{
		string url = ApiRoute.SIGNUP_PAGE_URL;

		Application.OpenURL (url);

		return url;
	}

	public static string toSignUpDone()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "SignUpDone";
//		if (isTablet()) {
//			sceneName = "PCSignUpDone";
//		}
		#else
		string sceneName = "PCSignUpDone";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string toSelectStories(string selectedTheme = "")
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "SelectStories";
//		if (isTablet()) {
//			sceneName = "PCSelectStories";
//		}
		#else
		string sceneName = "PCSelectStories";
		#endif

		if (selectedTheme != "") {
			PhoneStoriesViewController.selectedTheme = selectedTheme;
		}

		Debug.Log ("Selected Theme: " + selectedTheme);

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

    public static string ToAllStories()
    {
        string sceneName = "AllStories";
        SceneManager.LoadScene(sceneName);
        return "AllStories";
    }


    public static string toHome()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "StemAdventures";
//		if (isTablet()) {
//			sceneName = "PCHome";
//		}
		#else
		string sceneName = "PCHome";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string toDownloadStory()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "DownloadStory";
//		if (isTablet()) {
//			sceneName = "PCDownloadStory";
//		}
		#else
		string sceneName = "PCDownloadStory";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string toWriteStory()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "PhoneWriteStory";
//		if (isTablet()) {
//			sceneName = "PCWriteStory";
//		}
		#else
		string sceneName = "PCWriteStory";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string toRateApp()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "PhoneRateApp";
//		if (isTablet()) {
//			sceneName = "PCRateApp";
//		}
		#else
		string sceneName = "PCRateApp";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string ToRateStory()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "PhoneRateStory";
//		if (isTablet()) {
//			sceneName = "PCRateStory";
//		}
		#else
		string sceneName = "PCRateStory";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string toAboutApp()
	{
		#if UNITY_ANDROID || UNITY_IOS
		string sceneName = "PhoneAbout";
//		if (isTablet()) {
//			sceneName = "PCAbout";
//		}
		#else
		string sceneName = "PCAbout";
		#endif

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}

	public static string toGameplay(Story story = null)
	{
		
		string sceneName = "PhoneGameplay";

		if (story != null) {
		//	GameplayViewController.SetSelectedStory (story);
		}

		SceneManager.LoadScene (sceneName);
		return sceneName;
	}
	
	//calculate physical inches with pythagoras theorem
	public static float DeviceDiagonalSizeInInches ()
	{
		float screenWidth = Screen.width / Screen.dpi;
		float screenHeight = Screen.height / Screen.dpi;
		float diagonalInches = Mathf.Sqrt (Mathf.Pow (screenWidth, 2) + Mathf.Pow (screenHeight, 2));

		Debug.Log ("Getting device inches: " + diagonalInches);

		return diagonalInches;
	}

	#if UNITY_ANDROID || UNITY_IOS
	public static bool isTablet()
	{
		return DeviceDiagonalSizeInInches () > 6.5f;
	}
	#endif
}

