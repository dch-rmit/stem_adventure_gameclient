﻿using System.IO;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public static class ImageExtensions
{

	public static void setImagePath(this Image image, string imagePath)
	{
		Texture2D texture = new Texture2D(100, 100);
		byte[] bytes = File.ReadAllBytes(imagePath);

		texture.LoadImage(bytes);
		texture.name = imagePath;

		Sprite sprite = Sprite.Create(texture, 
			new Rect(0, 0, texture.width, texture.height),new Vector2(0,0), 100);

		image.sprite = sprite;
	}
	
	public static bool GetImagePath(string imagePath)
	{
		return File.Exists(imagePath);
	}
	
	

	public static IEnumerator setImageURL(this Image image, string url)
	{
		Debug.Log ("Downloaded Image URL - " + url);

		var tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
		
		using (WWW www = new WWW(url))
		{
			yield return www;
			www.LoadImageIntoTexture(tex);
//			image.GetComponent<Renderer>().material.mainTexture = tex;

			Debug.Log ("tex = " + tex);
			Debug.Log ("image = " + image);

			image.sprite = Sprite.Create (tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f));
		}
	}
	
	private static Image CreateTempChildImage(Image image)
    {
        Image result = null;

        string tempChildName = GetTempChildName(image);
        Transform foundTransform = image.transform.Find(tempChildName);
        GameObject tempGo = foundTransform != null ? foundTransform.gameObject : null;

        if (tempGo == null)
        {
            tempGo = new GameObject("TempCloneChild");
            var rt = image.GetComponent<RectTransform>();

            var rtPrime = tempGo.AddComponent<RectTransform>();
            rtPrime.SetParent(rt);
            rtPrime.localScale = Vector3.one;
            rtPrime.anchorMin = Vector2.zero;
            rtPrime.anchorMax = Vector2.one;
            rtPrime.sizeDelta = Vector2.zero;
            rtPrime.anchoredPosition = Vector2.zero;

            result = tempGo.AddComponent<Image>();
            result.preserveAspect = image.preserveAspect;
        }
        else
        {
            result = tempGo.GetComponent<Image>();
        }

        return result;
    }

    private static string GetTempChildName(Image target) => string.Format("TempCloneChild_{0}", target.GetInstanceID());

    public static float GetAlpha(this Image image) => image.color.a;
    public static void SetAlpha(this Image image, float alpha)
    {
        var color = image.color;
        color.a = alpha;
        image.color = color;
    }


    private static void RemoveTempChildImage(Image childImage)
    {
        if (childImage != null)
        {
            Object.Destroy(childImage.gameObject);
        }
    }

    public static Tweener DOCrossfadeImage(this Image image, Sprite to, float duration, System.Action OnComplete = null)
    {
        Image childImage = CreateTempChildImage(image);
        float progress = 0f;
        const float finalAlpha = 1f;

        childImage.SetAlpha(0f);
        childImage.sprite = to;

        return DOTween.To(
            () => progress,
            (curProgress) =>
            {
                progress = curProgress;

                float childAlpha = finalAlpha * progress;
                float imageAlpha = finalAlpha - childAlpha;
                image.SetAlpha(imageAlpha);
                childImage.SetAlpha(childAlpha);
            },
            1f, duration)
            .OnComplete(() => 
            {
                image.sprite = to;
                image.SetAlpha(childImage.GetAlpha());

                RemoveTempChildImage(childImage);

                OnComplete?.Invoke();
            })
            .OnKill(() =>
            {
                //Note: If you expect this tween will cancel and wish to halt the
                //  animation, remove this next line. It will look bad when you 
                //  start another CrossFadeImage animation on this
                RemoveTempChildImage(childImage);
            });
    }

	public static void saveImage(Image image)
	{
		
	}
}

