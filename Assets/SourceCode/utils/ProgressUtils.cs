﻿using System;
using UnityEngine.SceneManagement;

public class ProgressUtils
{
	public ProgressUtils ()
	{
	}

	public static void showProgressDialog()
	{
		SceneManager.LoadScene("ProgressDialog", LoadSceneMode.Additive);
	}

	public static void dismissProgressDialog()
	{
		SceneManager.UnloadSceneAsync ("ProgressDialog");
	}
}

