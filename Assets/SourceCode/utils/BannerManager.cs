﻿using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace SourceCode.utils
{
	public class BannerManager
	{
		public const string ROOT_BANNER_FOLDER_NAME = "/Banners";

		public bool SaveDownloadedAdImage(UnityWebRequest w, Banner banner)
		{
			string saveToFolderPath = GetRootBannerFolderPath ();
			string filePath = banner.filePath ;

			Debug.Log("Save AD file path: " + filePath);

			// Create the folder for this story if not existed
			if (!Directory.Exists (saveToFolderPath)) {
				Directory.CreateDirectory (saveToFolderPath);
			}

			// Delete old version if existed
			if (File.Exists(filePath)) {
				File.Delete (filePath);
			}
 
			return FileUtils.byteArrayToFile (filePath, w.downloadHandler.data);
		}

		public void DeleteRootBannerFolder()
		{
			string folder = GetRootBannerFolderPath ();

			if (Directory.Exists(folder)) {
				Directory.Delete (folder, true);
			}
		}

		public string GetFilePathOfBanner(Banner banner)
		{
			return GetRootBannerFolderPath () + "/" + banner.fileName + "." + banner.fileType;
		}

		public DirectoryInfo GetRootBannerFolder()
		{
			string rootBannerFolderPath = GetRootBannerFolderPath ();
			DirectoryInfo di;

			// Create the all stories folder if not existed
			if (!Directory.Exists (rootBannerFolderPath)) {
				di = Directory.CreateDirectory (rootBannerFolderPath);
			} else {
				di = new DirectoryInfo (rootBannerFolderPath);
			}

			return di;
		}

		public string GetRootBannerFolderPath()
		{
			return Application.persistentDataPath + ROOT_BANNER_FOLDER_NAME;
		}
	}
}

