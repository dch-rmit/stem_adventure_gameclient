﻿using System;
using UnityEngine;

public class PrefUtils
{
	public static string USER_ID = "USER_ID";
	public static string SESSION = "SESSION";
	public static string BANNER_LAST_UPDATE = "BANNER_LAST_UPDATE";

	public static string PREFIX_DOWNLOADED_STORY = "PREFIX_DOWNLOADED_STORY_";
	public static string PREFIX_FULL_MARK_ = "PREFIX_FULL_MARK_";

	public PrefUtils ()
	{
	}

	public static void saveFullMarkStoryAttempt(int storyId, int userId, int marks)
	{
		PlayerPrefs.SetInt (PREFIX_FULL_MARK_ + storyId + "_" + userId, marks);
	}

	public static int loadFullMarkStoryAttempt(int storyId, int userId)
	{
		string key = PREFIX_FULL_MARK_ + storyId + "_" + userId;
		return PlayerPrefs.GetInt (key, -1);
	}

	public static void saveDownloadedStoryLastUpdateTime(int storyId, string lastUpdate)
	{
		PlayerPrefs.SetString (PREFIX_DOWNLOADED_STORY + storyId, lastUpdate);
	}

	public static string loadDownloadedStoryLastUpdateTime(int storyId)
	{
		string key = PREFIX_DOWNLOADED_STORY + storyId;
		return PlayerPrefs.GetString (key);
//		return Convert.ToInt64(PlayerPrefs.GetString (key));
	}

	public static void saveBannerLastUpdateTime(string lastUpdate)
	{
		PlayerPrefs.SetString (BANNER_LAST_UPDATE, lastUpdate);
	}

	public static string loadBannerLastUpdateTime()
	{
		return PlayerPrefs.GetString (BANNER_LAST_UPDATE, "-1");
	}

	public static long loadBannerLastUpdateTimeAsLong()
	{
		string bannerLastUpdateStr = loadBannerLastUpdateTime ();
		return bannerLastUpdateStr == "" ? 0 : Convert.ToInt64 (bannerLastUpdateStr);
	}

	public static void saveUserId(int userId)
	{
		PlayerPrefs.SetInt (USER_ID, userId);
	}

	public static int loadUserId()
	{
		if (!PlayerPrefs.HasKey (USER_ID)) {
			return -1;
		}

		return PlayerPrefs.GetInt (USER_ID);
	}

	public static void saveSession(string session)
	{
		PlayerPrefs.SetString (SESSION, session);
	}

	public static string loadSession()
	{
		return PlayerPrefs.GetString (SESSION);
	}

	// AKA sign out
	public static void clearSessionAndUserId()
	{
		saveUserId (-1);
		saveSession ("");
	}

	public static bool isSignedIn()
	{
		return loadSession ().Length > 0;
	}

	public static void save()
	{
		PlayerPrefs.Save ();
	}
}

