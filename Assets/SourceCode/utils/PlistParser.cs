﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SourceCode.utils
{
	public class PlistParser
	{
		public Story ParseStory(string storyFilePath, DirectoryInfo storyFolder)
		{
			string plistFileName = Path.GetFileNameWithoutExtension (storyFilePath);
			PlistDocument document = new PlistDocument ();
			document.ReadFromFile (storyFilePath);

			// Get story name, background image path, preface, and starting stage ID
			string storyName = document.root [PlistStoryKey.KEY_STORY_NAME].AsString();
			string authorName = document.root [PlistStoryKey.KEY_AUTHOR_NAME].AsString();
			string version = document.root [PlistStoryKey.KEY_VERSION].AsString();
			string theme = document.root [PlistStoryKey.KEY_THEME].AsString();
			string backgroundImagePath = document.root [PlistStoryKey.KEY_BACKGROUND_IMAGE_PATH].AsString();
			string preface = document.root [PlistStoryKey.KEY_PREFACE].AsString();
			string startStageId = document.root [PlistStoryKey.KEY_START_STAGE_ID].AsString();
			string keyLearning = document.root[PlistStoryKey.KeyLearning].AsString();
			Debug.Log(keyLearning);

			string realBackgroundImagePath = "";
			if (backgroundImagePath != "") {
				FileInfo[] backgroundImageFiles = storyFolder.GetFiles (backgroundImagePath);

				if (backgroundImageFiles.Length > 0) {
					realBackgroundImagePath = backgroundImageFiles [0].FullName;
				}
			}

			PlistElementDict stagesPlist = document.root [PlistStoryKey.KEY_STAGES].AsDict ();
			Dictionary<string, Stage> stages = new Dictionary<string, Stage>();

//		int maxMarks = 0;
			int maxMarks = document.root [PlistStoryKey.KEY_MAX_MARKS].AsInteger();
			int longestDescLength = 0;
			int longestSelLength = 0;

			// Loop through each plist stage to get data for stage objects
			foreach(KeyValuePair<string, PlistElement> entry in stagesPlist.values)
			{
				PlistElementDict dict = entry.Value.AsDict ();

				string stageName = dict [PlistStoryKey.KEY_STAGE_NAME].AsString ();
				string description = dict [PlistStoryKey.KEY_DESCRIPTION].AsString ();
				Stage.Image[] images = new Stage.Image[0];

				PlistElement imagesPlist = dict [PlistStoryKey.KEY_IMAGES];
				if(imagesPlist != null) { // There are some images available for this stage
					PlistElementArray imageArrayPlist = imagesPlist.AsArray ();
					images = new Stage.Image[imageArrayPlist.values.Count];

					for (int i = 0; i < images.Length; i++) {
						PlistElementDict imageDict = imageArrayPlist.values [i].AsDict ();
						string imageName = null;
						try
						{
							imageName = imageDict[PlistStoryKey.KEY_IMAGE_PATH].AsString();
						}
						catch (	NullReferenceException e)
						{
							Console.WriteLine("File not found");
							continue;
						}

						FileInfo[] imageFiles = storyFolder.GetFiles (imageName);

						if (imageFiles.Length > 0) {
							string base64String = "";

//						if (imageDict.values.ContainsKey (PlistStoryKey.KEY_IMAGE_BASE64_STRING)) {
//							base64String = imageDict [PlistStoryKey.KEY_IMAGE_BASE64_STRING].AsString();
//						}

							string imagePath = imageFiles [0].FullName;
//						int x = imageDict [PlistStoryKey.KEY_X].AsInteger ();
//						int y = imageDict [PlistStoryKey.KEY_Y].AsInteger ();
							int x = 0;
							int y = 0;
							images [i] = new Stage.Image (base64String, imagePath, x, y);
						}

//					PlistElementDict imageDict = imageArrayPlist.values [i].AsDict ();
//					string imageName = imageDict[PlistStoryKey.KEY_IMAGE_PATH].AsString ();
//
//					string base64String = "";
//
//					if (imageDict.values.ContainsKey (PlistStoryKey.KEY_IMAGE_BASE64_STRING)) {
//						base64String = imageDict [PlistStoryKey.KEY_IMAGE_BASE64_STRING].AsString();
//					}
//
//					int x = imageDict [PlistStoryKey.KEY_X].AsInteger ();
//					int y = imageDict [PlistStoryKey.KEY_Y].AsInteger ();
//					images [i] = new Stage.Image (base64String, imageName, x, y);
					}
				}


				PlistElementArray selectionPlist = dict [PlistStoryKey.KEY_SELECTIONS].AsArray ();
				StageSelection[] selections = new StageSelection[selectionPlist.values.Count];

				// Get selections for current stage
				for (int i = 0; i < selections.Length; i++) {
					PlistElementDict selectionDict = selectionPlist.values [i].AsDict ();
					string selectionText = selectionDict[PlistStoryKey.KEY_SELECTION_TEXT].AsString ();
					string nextStageId = selectionDict[PlistStoryKey.KEY_NEXT_STAGE_ID].AsString ();
					int marks = selectionDict [PlistStoryKey.KEY_MARKS].AsInteger ();
					selections [i] = new StageSelection (selectionText, marks, nextStageId);

					// Add marks of this selection to maximum marks of the story
//				if (marks > 0) {
//					maxMarks += marks;
//				}

					if (selectionText.Length > longestSelLength) {
						longestSelLength = selectionText.Length;
					}
				}

				Stage stage = new Stage (entry.Key, stageName, description, images, selections);

				stages.Add (entry.Key, stage);



				if (description.Length > longestDescLength) {
					longestDescLength = description.Length;
				}
			}

//		Debug.Log ("Longest Description Length: " + longestDescLength);
//		Debug.Log ("Longest Selection Length: " + longestSelLength);

			Story story = new Story (storyName, authorName, version, theme, maxMarks,
				realBackgroundImagePath, preface, startStageId, stages, keyLearning);

			// Parse story ID
			int storyId = 0;
			if (int.TryParse(plistFileName, out storyId)) {
				story.id = storyId;
			}

			return story;
		}
	}
}

 