﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

static class CanvasExtensions {
	
//	public static Vector2 SizeToParent(this RawImage image, float padding = 0) {
//		var parent = image.transform.parent.GetComponentInParent<RectTransform>();
//		var imageTransform = image.GetComponent<RectTransform>();
//
//		if (!parent) 
//		{ 
//			return imageTransform.sizeDelta; 
//		} //if we don't have a parent, just return our current width;
//
//		padding = 1 - padding;
//		float w = 0, h = 0;
//		float ratio = image.texture.width / (float)image.texture.height;
//		var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
//
//		if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90) {
//			//Invert the bounds if the image is rotated
//			bounds.size = new Vector2(bounds.height, bounds.width);
//		}
//
//		//Size by height first
//		h = bounds.height * padding;
//		w = h * ratio;
//		if (w > bounds.width * padding) { //If it doesn't fit, fallback to width;
//			w = bounds.width * padding;
//			h = w / ratio;
//		}
//
//		imageTransform.sizeDelta = new Vector2(w, h);
//
//		return imageTransform.sizeDelta;
//	}

	public static Vector2 SizeToParent2(this RawImage image, float canvasWidth, float canvasHeight, float padding = 0) {
		float w = 0, h = 0;
		var parent = image.GetComponentInParent<RectTransform>();
		var imageTransform = image.GetComponent<RectTransform>();

		// check if there is something to do
		if (image.texture != null) {
			
			if (!parent) {  //if we don't have a parent, just return our current width;
				Debug.Log("howdy");
				return imageTransform.sizeDelta; 
			}

			Debug.Log ("PARENT - width: " + canvasWidth);
			
			float aspectRatio = image.texture.height / (float)image.texture.width;
			float maxWidth = (aspectRatio >= 1.4f) ? canvasWidth - 64 : canvasWidth - 16;
			float heightScale = maxWidth / (float)image.texture.width;
			w = maxWidth;
			h = heightScale * image.texture.height;
			
		}

		imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
		imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
		return imageTransform.sizeDelta;
	}
}


