﻿using UnityEngine.UI;

namespace SourceCode.utils
{
	public static class ButtonExtensions
	{
	
		public static void setText(this Button button, string text)
		{
			Text textView = button.transform.Find("Text").GetComponent<Text>();
			textView.text = text;
		}
	}
}

