﻿using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using SourceCode.utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

/*
    PhoneHomeRevised Controller class. This is the controller for the PhoneHomeRevised screen.

*/
namespace SourceCode
{
    public class PhoneHomeRevisedController : MonoBehaviour
    {

        private const int ScrollInterval = 5;
        private const float ScrollAnimationDuration = 0.5f;

        public RectTransform bannerImagePanel;
        public RectTransform storyNamePanel;
        public Text storyNameLabel;
        public Image sampleTopStoryImage;

        public RectTransform paginationPanel;
        private List<Image> topStoryImages;
        private int currentTopStoryIndex;
        private int nextTopStoryIndex;
        private bool autoScrollPaused;
        private bool animationRunning;

        private List<Story> topStories;
        private List<Banner> banners;

        public Text topTitleView;
        
        public Button playButton;
        
        public Image[] themes;

        public HorizontalScrollSnap horizontalScrollSnap;
        
        void Start()
        {
            //Lock to Portrait mode for this screen only. Layout looks weird with landscape.
            Screen.orientation = ScreenOrientation.Portrait;
            
            banners = new List<Banner>();

            //LoadTopStories();
            //SetupTopStoryView();
            //LoadAds();
            SetUpBodyView();
            //UpdateRelatingTextViews();

            if (topTitleView != null)
            {
                topTitleView.gameObject.SetActive(false);
            }

            InvokeRepeating(nameof(ScrollToNextImage), ScrollInterval, ScrollInterval);
            
            
            
        }


        private void LoadTopStories()
        {
            topStories = new StoryManager().GetTopStoriesFromStoryList();
        }

        private void LoadAds()
        {
            ApiService service = new ApiService();

            // Get JSON of list of ads
            StartCoroutine(service.DoGetMethod(ApiRoute.ALL_ADS_URL,
                (w) => { // On success
                    string jsonString = w.downloadHandler.text;
                    JSONNode json = JSON.Parse(jsonString);
                    JSONArray bannersJson = json[ApiRoute.KEY_BANNERS].AsArray;
                    long latestBannerUpdate = PrefUtils.loadBannerLastUpdateTimeAsLong();
                    long newLastUpdate = 0;

                    Debug.Log("Saved Last Update: " + latestBannerUpdate);

                    // Parse JSON array
                    for (int i = 0; i < bannersJson.Count; i++)
                    {
                        JSONNode bannerJson = bannersJson[i];
                        Banner banner = new Banner(bannerJson);

                        Debug.Log("ID: " + banner.id + " - FN: " + banner.fileName + " - Last Update: " + banner.lastUpdate);

                        // Check for new last update time
                        if (latestBannerUpdate < banner.lastUpdate)
                        {
                            newLastUpdate = banner.lastUpdate;
                        }

                        banners.Add(banner);

                        // Prepare corresponding image view
                        Image imageView = SetupImageView();
                      

                        imageView.gameObject.SetActive(false);
                       

                        topStoryImages.Add(imageView);
                    }

                    Debug.Log("Picked Last Update: " + newLastUpdate);

                    // Need update, reload the whole ads feature
                    if (newLastUpdate > latestBannerUpdate)
                    {
                        BannerManager bannerManager = new BannerManager();

                        bannerManager.DeleteRootBannerFolder();
                        PrefUtils.saveBannerLastUpdateTime("" + newLastUpdate);

                        // Download each ads
                        for (int i = 0; i < banners.Count; i++)
                        {
                            DownloadAdImage(i);
                        }
                    }
                    else
                    { // Load current ads
                        for (int i = 0; i < banners.Count; i++)
                        {
                            SetUpAdView(i);
                        }
                    }
                },
                (w) => { // On error

                }));
        }
        
        //Alternative to loading Banner ads for stories.
        private void LoadThemes()
        {
            int bannerIndex = 4;
            Banner banner = banners[bannerIndex];
            Image imageView = topStoryImages[bannerIndex];
            Button imageButton = imageView.GetComponent<Button>();

            if (banner.filePath != "")
            {
                imageView.setImagePath(banner.filePath);
            }

            Destroy(imageButton);
            imageView.gameObject.SetActive(false);
        }

        private void DownloadAdImage(int bannerIndex)
        {
            Banner banner = banners[bannerIndex];
            ApiService service = new ApiService();

            StartCoroutine(service.DownloadAdImage(this, banner,
                (w) => { // On success
                    SetUpAdView(bannerIndex);
                },
                (w) => { // On error

                }));
        }

        private void SetupTopStoryView()
        {
            topStoryImages = new List<Image>();
            Sprite defaultImageSprite = sampleTopStoryImage.sprite;

            for (int i = 0; i < topStories.Count; i++)
            {
                Story story = topStories[i];

                Image imageView;
                Button imageButon;

                if (i == 0)
                {
                    imageView = sampleTopStoryImage;
                    imageButon = imageView.gameObject.AddComponent<Button>();

                }
                else
                {
                    imageView = SetupImageView();
                    imageButon = imageView.GetComponent<Button>();

                    imageView.gameObject.SetActive(false);
                }

                topStoryImages.Add(imageView);

                int index = i;

                // If story is null, set default image and click function
                if (story == null)
                {
                    imageView.sprite = defaultImageSprite;

                    imageButon.onClick.AddListener(() => {
                        Router.toDownloadStory();
                    });

                    continue;
                }

                // If the cover image path of the story is available, set the image to the image view
                if (story.backgroundImagePath != "")
                {
                    imageView.setImagePath(story.backgroundImagePath);
                }
                else
                { // Otherwise, set default image according to the story's theme
                    StoryManager sm = new StoryManager();
                    imageView.setImagePath(sm.getPathToThemeImage(story.theme));
                }

                imageButon.onClick.AddListener(() => {
                    Router.toGameplay(story);
                });
            }

            currentTopStoryIndex = 0;
            UpdateRelatingTextViews();
        }

        void SetUpAdView(int bannerIndex)
        {
            int realIndex = topStories.Count + bannerIndex;
            Banner banner = banners[bannerIndex];
            Image imageView = topStoryImages[realIndex];
            Button imageButton = imageView.GetComponent<Button>();

            if (!File.Exists(banner.filePath))
            {
                DownloadAdImage(bannerIndex);
                return;
            }

            if (banner.filePath != "")
            {
                imageView.setImagePath(banner.filePath);
            }

            Destroy(imageButton);
            imageView.gameObject.SetActive(false);
            
        }

        Image SetupImageView()
        {
            Image imageView = Instantiate(sampleTopStoryImage, sampleTopStoryImage.transform.position, sampleTopStoryImage.transform.rotation);
            imageView.transform.SetParent(bannerImagePanel, false);

            return imageView;
        }

        void ScrollToImage(int index)
        {
            animationRunning = true;

            nextTopStoryIndex = index;
            Image currentImage = topStoryImages[currentTopStoryIndex];
            Image nextImage = topStoryImages[index];

            nextImage.gameObject.SetActive(true);
            nextImage.canvasRenderer.SetAlpha(0.0f);

            currentImage.CrossFadeAlpha(0.0f, ScrollAnimationDuration, false);
            nextImage.CrossFadeAlpha(1.0f, ScrollAnimationDuration, false);

            Invoke("UpdateOnNextImageDone", ScrollAnimationDuration);
        }

        // Scrolls the Horizontal Scroll Snap GameObject to the next Image. If at end goes back to first one.
        private void ScrollToNextImage()
        {
            horizontalScrollSnap.NextScreen();
            if (horizontalScrollSnap._currentPage + 1 == horizontalScrollSnap.ChildObjects.Length)
            {
                horizontalScrollSnap.GoToScreen(0);
            }
        }

        void UpdateOnNextImageDone()
        {
            Image currentImage = topStoryImages[currentTopStoryIndex];
            Image nextImage = topStoryImages[nextTopStoryIndex];
            Debug.Log("SCROLL - from " + currentTopStoryIndex + " to " + nextTopStoryIndex);

            currentImage.gameObject.SetActive(false);

            currentTopStoryIndex = nextTopStoryIndex;
            animationRunning = false;

            UpdateRelatingTextViews();
        }

        private void UpdateRelatingTextViews()
        {
            if (currentTopStoryIndex < topStories.Count && topStories[currentTopStoryIndex] != null)
            {
                storyNamePanel.gameObject.SetActive(true);
                storyNameLabel.GetComponent<Text>().text = topStories[currentTopStoryIndex].name;
            }
            else
            {
                storyNamePanel.gameObject.SetActive(false);
            }
        }

        private int getNextStoryIndex()
        {
            if (currentTopStoryIndex < topStoryImages.Count - 1)
            {
                return currentTopStoryIndex + 1;
            }

            return 0;
        }

        private void SetUpBodyView()
        {
            playButton.onClick.AddListener(GoToSelectStories);
        }

        //Send user to screen with all stories. Validate that stories exist first.
        private void GoToSelectStories()
        {

            //StoryManager storyManager = new StoryManager();
            List<Story> stories = new StoryManager().GetAllStories();

            if (stories.Count == 0)
            {

                const string message = "There is no stories downloaded. Please check download page for stories.";
                MessageDialogViewController.show(message);
                return;
            }
            
            Router.ToAllStories();
        }

        private void LoadThemeImages()
        {
//            themes = new Image[4];
//            foreach (var theme in themes)
//            {
//                theme.setImagePath();
//            }
        }
        
    }
}
