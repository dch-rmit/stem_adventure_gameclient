﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeSceneButton : MonoBehaviour {

	public LoadSceneMode loadSceneMode = LoadSceneMode.Single;
	public string sceneName = "";
	public string buttonText = "";

	// Use this for initialization
	void Start () {
		this.GetComponentInChildren<Text>().text = buttonText;
		this.GetComponent<Button>().onClick.AddListener(() => {
			SceneManager.LoadScene(sceneName, loadSceneMode);
		});
	}

	// Update is called once per frame
	void Update () {

	}
}