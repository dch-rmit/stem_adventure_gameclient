using System;

public class Identity
{
    public string userName;
    public string userPassword;
    public int identity;

    public Identity(string userName, string userPassword, int identity)
	{
        this.userName = userName;
        this.userPassword = userPassword;
        this.identity = identity;
	}
}
